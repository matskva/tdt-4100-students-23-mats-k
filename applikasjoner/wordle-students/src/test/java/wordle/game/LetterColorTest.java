package wordle.game;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;

public class LetterColorTest {
    @Test

    @DisplayName("Test constructor")
    public void testConstructor() {
        LetterColor color = new LetterColor('a', LetterColor.CORRECT);

        Assertions.assertEquals('a', color.getLetter(), "Wrong letter");
        Assertions.assertEquals(LetterColor.CORRECT, color.getColor(), "Wrong color");

    }

    @Test
    @DisplayName("Check encapsulation, invalid color throws exception")
    public void testInvalidColor() {
        assertThrows(IllegalArgumentException.class, () -> {
            new LetterColor('a', 'k');
        });
    }

}
