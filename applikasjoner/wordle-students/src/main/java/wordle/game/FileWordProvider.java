package wordle.game;

import java.util.List;
import java.util.Random;

import wordle.util.FileHelper;

import java.io.IOException;
import java.util.ArrayList;

public class FileWordProvider implements WordProvider {

    private final List<String> words;
    private final Random random;

    public FileWordProvider() throws IOException {
        this("wordle/words.txt", true);
    }

    public FileWordProvider() throws IOException {
        this.words = FileHelper.readLines("/world/words.txt", true);
        this.random = new Random();

    }

    public List<String> getWords() {
        return words;
    }

    @Override
    public String getNextWord() {
        int index = this.random.nextInt(words.size());
        return this.words.get(index);
    }
}
