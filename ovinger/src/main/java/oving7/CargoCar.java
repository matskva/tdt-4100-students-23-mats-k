package oving7;

public class CargoCar extends TrainCar {

    int deadWeight;
    int cargo;

    CargoCar(int deadWeight, int cargo) {

        super(deadWeight);

        super.weight = deadWeight + cargo;
        this.deadWeight = deadWeight;
        this.cargo = cargo;

    }

    public int getCargoWeight() {

        return this.cargo;
    }

    public void setCargoWeight(int weight) {
        this.cargo = weight;
        super.weight = deadWeight + cargo;

    }

}
