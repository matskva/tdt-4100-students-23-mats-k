package oving7;

public class SavingsAccount2 extends AbstractAccount {

    int withdrawals;
    double fee;

    SavingsAccount2(int with, double fee) {
        this.withdrawals = with;
        this.fee = fee;
    }

    @Override
    void internalWithdraw(double amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Amount cant be negative!");
        } else if (super.balance - amount < 0) {
            throw new IllegalStateException("Cant withdraw more than you have");
        } else if (super.balance - (amount + fee) < 0) {
            throw new IllegalStateException();
        }

        else if (withdrawals <= 0) {
            super.balance -= (amount + fee);
        } else {
            super.balance -= amount;
            withdrawals--;
        }

    }

}
