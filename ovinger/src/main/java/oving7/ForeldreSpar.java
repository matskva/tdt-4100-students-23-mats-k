package oving7;

import java.util.ArrayList;

public class ForeldreSpar extends SavingsAccount {
    protected int withdrawals;
    protected int maxWithdrawals;

    public ForeldreSpar(double rent, int max) {
        super(rent);
        this.maxWithdrawals = max;
        withdrawals = 0;
    }
y

    public void withdraw(double amount) {

        if (withdrawals == maxWithdrawals) {
            throw new IllegalStateException("Dont have any remaining withdrawals");
        }

        this.withdrawals += 1;
        super.withdraw(amount);

    }

    public void deposit(double amount) {
        super.deposit(amount);
    }

    public int getRemainingWithdrawals() {
        return maxWithdrawals - withdrawals;
    }

    public void endYearUpdate() {
        withdrawals = 0;
        super.endYearUpdate();
    }

    public static void main(String[] args) {
        ForeldreSpar fs = new ForeldreSpar(0.05, 3);

        fs.deposit(10000);

        fs.withdraw(200);
        System.out.println(fs.getRemainingWithdrawals());

        fs.withdraw(200);
        System.out.println(fs.getRemainingWithdrawals());

    }

}
