package oving7;

public class SavingsAccount implements Account {

    protected double balance;
    protected double rent;

    public SavingsAccount(double rent) {
        this.balance = 0;
        this.rent = rent;

    }

    public void deposit(double amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Negativt beløp ikke lov");
        }

        this.balance += amount;
    }

    public void withdraw(double amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Negativt beløp ikke lov");
        }
        if (this.balance - amount < 0) {
            throw new IllegalStateException("Kan ikke ta ut så mye penger");
        }

        this.balance -= amount;

    }

    public double getBalance() {
        return this.balance;
    }

    public void endYearUpdate() {

        this.balance = this.balance * (1 + rent);

    }

}
