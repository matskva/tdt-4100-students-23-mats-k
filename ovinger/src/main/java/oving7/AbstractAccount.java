package oving7;

abstract class AbstractAccount {

    protected double balance;

    AbstractAccount() {
        this.balance = 0;
    }

    public void deposit(double amount) {

        if (amount < 0) {
            throw new IllegalArgumentException("Cant deposit a negative amount");
        }
        this.balance += amount;
    }

    public void withdraw(double amount) {

        if (amount < 0) {
            throw new IllegalArgumentException("Cant withdraw a negative amount");
        }

        internalWithdraw(amount);
    }

    abstract void internalWithdraw(double amount);

    public double getBalance() {
        return this.balance;
    }

}
