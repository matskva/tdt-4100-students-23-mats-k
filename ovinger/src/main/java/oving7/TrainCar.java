package oving7;

public class TrainCar {

    public int weight;
    public int deadWeight;

    TrainCar(int weight) {
        this.weight += weight;
    }

    public int getTotalWeight() {

        return weight;
    }

    public void setDeadWeight(int deadWeight) {

        this.weight = deadWeight;
        this.deadWeight = deadWeight;
    }

    public int getDeadWeight() {

        return deadWeight;
    }

}
