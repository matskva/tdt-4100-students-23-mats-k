package oving7;

public class PassengerCar extends TrainCar {

    int deadWeight;
    int passengers;
    int passengerWeight;

    PassengerCar(int weight, int passengers) {

        super(weight);
        this.passengerWeight = (passengers * 80);

        super.weight += deadWeight + passengerWeight;
        this.passengers = passengers;
    }

    public int getPassengerCount() {
        return this.passengers;
    }

    public void setPassengerCount(int passengers) {
        this.passengers = passengers;
        super.weight -= passengerWeight;
        this.passengerWeight = (passengers * 80);
        super.weight += passengerWeight;
    }
}
