package oving7;

public class BSU extends SavingsAccount {

    protected double maxBalance;
    protected double totalBalance;

    public BSU(double rent, double max) {
        super(rent);
        this.maxBalance = max;
        this.totalBalance = 0;
    }

    public double getTaxDeduction() {

        return this.balance * 0.2;
    }

    public void deposit(double amount) {

        if (balance + amount > maxBalance) {
            throw new IllegalStateException("Du kan ikke legge til mer i år");
        }

        super.deposit(amount);
    }

    public void withdraw(double amount) {
        super.withdraw(amount);
    }

    public void endYearUpdate() {

        super.endYearUpdate();
        totalBalance += balance;
        balance = 0;

    }

    public double getBalance() {
        return this.totalBalance + balance;
    }

}
