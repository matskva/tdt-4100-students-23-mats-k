package oving7;

import java.util.ArrayList;

public class Train {

    private ArrayList<TrainCar> TrainCars = new ArrayList<TrainCar>();
    private ArrayList<CargoCar> CargoCars = new ArrayList<CargoCar>();
    private ArrayList<PassengerCar> passengerCars = new ArrayList<PassengerCar>();

    public void addTrainCar(TrainCar car) {

        TrainCars.add(car);

        if (car instanceof CargoCar) {
            this.CargoCars.add((CargoCar) car);
        } else if (car instanceof PassengerCar) {
            this.passengerCars.add((PassengerCar) car);
        }

    }

    public boolean contains(TrainCar car) {

        return TrainCars.contains(car);

    }

    public int getTotalWeight() {

        int weight = 0;

        for (TrainCar cars : TrainCars) {
            weight += cars.getTotalWeight();
        }

        return weight;
    }

    public int getPassengerCount() {
        int passCount = 0;

        for (PassengerCar cars : passengerCars) {
            passCount += cars.getPassengerCount();
        }

        return passCount;
    }

    public int getCargoWeight() {

        int cargoWeight = 0;

        for (CargoCar car : CargoCars) {
            cargoWeight += car.getCargoWeight();
        }

        return cargoWeight;
    }

    @Override
    public String toString() {
        return "Train [TrainCars=" + TrainCars + ", CargoCars=" + CargoCars + ", passengerCars=" + passengerCars + "]";
    }

}
