package oving7;

public class DebitAccount extends AbstractAccount {

    double balance;

    @Override
    void internalWithdraw(double amount) {
        this.balance = super.balance;

        if (super.balance - amount < 0) {
            throw new IllegalStateException("Innsufficient balance");
        }

        super.balance -= amount;

    }

}
