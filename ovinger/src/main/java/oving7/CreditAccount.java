package oving7;

public class CreditAccount extends AbstractAccount {

    double creditLine;

    CreditAccount(double credit) {
        this.creditLine = credit;
    }

    @Override
    void internalWithdraw(double amount) {
        if (amount < 0) {
            throw new IllegalArgumentException();
        } else if (super.balance + this.creditLine - amount < 0) {
            throw new IllegalStateException("You dont have enough credit!");
        }
        super.balance -= amount;
    }

    public double getCreditLine() {
        return creditLine;
    }

    public void setCreditLine(double creditLine) {

        if (creditLine < 0) {
            throw new IllegalArgumentException("CreditLine can't be negative");
        } else if (super.balance + creditLine < 0) {
            throw new IllegalStateException("CreditLine must cover debt");
        }
        this.creditLine = creditLine;
    }

}
