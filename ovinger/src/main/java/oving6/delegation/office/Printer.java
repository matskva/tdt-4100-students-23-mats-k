package oving6.delegation.office;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Printer {

    Employee employee;
    HashMap<Employee, List<String>> history = new HashMap<>();

    HashMap<Employee, List<String>> copyHistory;

    public void printDocument(String document, Employee employee) {
        List<String> dokument = new ArrayList<>();
        dokument.add(document);

        if (!history.containsKey(employee)) {
            history.put(employee, dokument);
        } else {
            history.get(employee).add(document);
        }

        this.copyHistory = history;

        System.out.println(document);
    }

    List<String> getPrintHistory(Employee employee) {

        if (history.get(employee) == null) {
            return new ArrayList<String>();
        }
        List<String> historyList = new ArrayList<>(copyHistory.get(employee)); // make a copy of the list associated
                                                                               // with the employee
        return historyList;
    }

    public static void main(String[] args) {
        Printer p1 = new Printer();
        Clerk c1 = new Clerk(p1);
        Clerk c2 = new Clerk(p1);

        p1.printDocument("Hello", c1);
        p1.printDocument("Hei", c2);
        p1.printDocument("Hallo", c1);

        p1.getPrintHistory(c1).remove("Hallo");

        System.out.println(p1.getPrintHistory(c1));

    }
}
