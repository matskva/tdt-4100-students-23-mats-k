package oving6.delegation.office;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.BinaryOperator;

public class Clerk implements Employee {

    Printer printer;
    int taskCount;

    Clerk(Printer printer) {

        this.printer = printer;
        this.taskCount = 0;

    }

    @Override
    public double doCalculations(BinaryOperator<Double> operation, double value1, double value2) {
        this.taskCount++;
        return operation.apply(value1, value2);

    }

    @Override
    public void printDocument(String document) {

        this.taskCount += 1;
        this.printer.printDocument(document, this);

    }

    @Override
    public int getTaskCount() {

        return this.taskCount;
    }

    @Override
    public int getResourceCount() {
        // TODO Auto-generated method stub
        return 1;
    }

}
