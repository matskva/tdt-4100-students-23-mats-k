package oving6.delegation.office;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.function.BinaryOperator;

public class Manager implements Employee {

    Collection<Employee> employees;
    int tasks;
    Iterator<Employee> it;

    Manager(Collection<Employee> employees) {

        if (employees.size() == 0) {
            throw new IllegalArgumentException("Cant give empty list");
        }

        this.employees = employees;
        this.it = employees.iterator();

    }

    @Override
    public double doCalculations(BinaryOperator<Double> operation, double value1, double value2) {

        if (!it.hasNext()) {
            it = employees.iterator();
        }
        tasks++;

        return it.next().doCalculations(operation, value1, value2);

    }

    @Override
    public void printDocument(String document) {

        if (!it.hasNext()) {
            it = employees.iterator();
        }
        tasks++;

        it.next().printDocument(document);

    }

    @Override
    public int getTaskCount() {
        return this.tasks;
    }

    @Override
    public int getResourceCount() {

        int resourceCount = 1;

        for (Employee e : employees) {
            resourceCount += e.getResourceCount();
        }

        return resourceCount;
    }

    public static void main(String[] args) {
        Printer p = new Printer();
        Collection<Employee> e1 = new ArrayList<>();
        e1.add(new Clerk(p));
        e1.add(new Clerk(p));
        e1.add(new Clerk(p));
        e1.add(new Clerk(p));
        e1.add(new Clerk(p));
        e1.add(new Clerk(p));
        e1.add(new Clerk(p));
        Manager manager = new Manager(e1);

        manager.doCalculations((x, y) -> x + y, 2, 5);
        manager.doCalculations((x, y) -> x * y, 2, 5);

        System.out.println(manager.getResourceCount());
        System.out.println(manager.getTaskCount());
    }

}
