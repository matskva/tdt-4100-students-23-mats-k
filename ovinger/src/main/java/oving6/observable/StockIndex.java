package oving6.observable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StockIndex implements StockListener {

    String name;
    double index;
    ArrayList<Stock> myStocks;

    StockIndex(String name, Stock... stocks) {

        myStocks = new ArrayList<>();

        this.name = name;

        if (stocks.length == 0) {
            this.index = 0;
        }

        for (int i = 0; i < stocks.length; i++) {
            myStocks.add(stocks[i]);
            stocks[i].addStockListener(this);
            this.index += stocks[i].getPrice();
        }
    }

    public void addStock(Stock stock) {

        if (myStocks.contains(stock)) {
            return;
        }

        this.myStocks.add(stock);
        this.index += stock.getPrice();
        stock.addStockListener(this);

    }

    public void removeStock(Stock stock) {

        if (!myStocks.contains(stock)) {
            return;
        }

        this.index -= stock.getPrice();
        stock.removeListener(this);
        myStocks.remove(stock);
    }

    public double getIndex() {
        return this.index;
    }

    @Override
    public void stockPriceChanged(Stock stock, double oldValue, double newValue) {

        index += newValue - oldValue;

    }

}
