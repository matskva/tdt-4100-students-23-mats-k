package oving6.observable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class HighscoreList {

    ArrayList<Integer> results;
    int listSize;
    ArrayList<HighscoreListListener> listeners;

    HighscoreList(Integer maxValue) {
        this.results = new ArrayList<>(maxValue);
        this.listSize = maxValue;
        listeners = new ArrayList<>();
    }

    public void addResult(Integer result) {

        int temp = 0;
        while (temp < results.size() && result >= results.get(temp)) {
            temp++;
        }

        if (results.size() == 0) {
            results.add(result);
            fireListChanged(temp);
            return;
        }

        if (results.size() == listSize) {
            if (result > results.get(results.size() - 1)) {
                return;
            }

            else {
                results.remove(results.size() - 1);
                fireListChanged(temp);
            }

        }

        results.add(result);
        Collections.sort(results);
        if (results.contains(result)) {
            fireListChanged(temp);
        }

    }

    public int size() {
        int size = 0;
        for (Integer i : results) {
            size++;
        }

        return size;

    }

    public int getElement(int position) {
        return results.get(position);
    }

    public void addHighscoreListListener(HighscoreListListener listner) {
        this.listeners.add(listner);
    }

    public void removeHighscoreListListener(HighscoreListListener listener) {
        this.listeners.remove(listener);
    }

    public void fireListChanged(int index) {
        this.listeners.forEach(l -> l.listChanged(this, index));
    }

    public static void main(String[] args) {
        HighscoreList h1 = new HighscoreList(3);

        h1.addResult(5);
        h1.addResult(6);
        h1.addResult(2);

        System.out.println(h1.results);

        h1.addResult(3);
        System.out.println(h1.results);

        // h1.addResult(7);
        // System.out.println(h1.results);

    }

}
