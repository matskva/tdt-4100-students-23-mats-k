package oving6.observable;

public interface HighscoreListListener {

    void listChanged(HighscoreList hsList, int index);
}
