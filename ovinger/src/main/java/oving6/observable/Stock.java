package oving6.observable;

import java.util.ArrayList;
import java.util.List;

public class Stock implements StockListener {

    String ticker;
    double price;
    ArrayList<StockListener> listeners;

    Stock(String ticker, double price) {
        this.ticker = ticker;
        this.price = price;
        this.listeners = new ArrayList<>();

    }

    public void setPrice(Double price) {

        if (price <= 0) {
            throw new IllegalArgumentException("price cant be negative");
        }

        for (StockListener stockListener : listeners) {

            stockListener.stockPriceChanged(this, getPrice(), price);

        }

        this.price = price;

    }

    public String getTicker() {
        return this.ticker;
    }

    public double getPrice() {
        return this.price;
    }

    void addStockListener(StockListener listener) {
        this.listeners.add(listener);
    }

    void removeListener(StockListener listener) {

        this.listeners.remove(listener);

    }

    @Override
    public void stockPriceChanged(Stock stock, double oldValue, double newValue) {

        setPrice(newValue);
    }

    public static void main(String[] args) {
        Stock s1 = new Stock("MMM", 0);
        s1.setPrice(-20.0);
    }
}
