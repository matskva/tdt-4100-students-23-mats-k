package oving2;

public class Vehicle {
    private char type;
    private char fuel;
    private String regnr;

    Vehicle(char type, char fuel, String regnr) {

        setVeicleType(type);
        setFuelType(fuel);
        setRegistrationNumber(regnr);
    }

    public void setFuelType(char fuel) {
        if (fuel != 'H' && fuel != 'E' && fuel != 'G' && fuel != 'D') {

            throw new IllegalArgumentException();
        }
        if (type == 'M' && fuel == 'H') {
            throw new IllegalArgumentException();
        } else {
            this.fuel = fuel;
        }
    }

    public char getFuelType() {

        return fuel;
    }

    public boolean checkForLetters(String letters) {

        return letters.matches("[a-zA-Z]+");

    }

    public boolean isDigit(String numbers) {
        return numbers.matches("[0-9]+");
    }

    public boolean isUpper(String letters) {
        return letters.toUpperCase().equals(letters);
    }

    public void setRegistrationNumber(String regnr) {

        String letters = regnr.substring(0, 2);
        String numbers = regnr.substring(2);

        if ((!checkForLetters(letters)) || !(isDigit(numbers))) {
            throw new IllegalArgumentException();
        }

        if (!isUpper(letters)) {
            throw new IllegalArgumentException();
        }

        if (fuel == 'E') {
            if (!(letters.contains("EL") || letters.contains("EK"))) {
                throw new IllegalArgumentException();
            }
        }
        if ((type == 'M' && numbers.length() != 4)) {
            throw new IllegalArgumentException();

        }

        if ((type == 'C' && numbers.length() != 5)) {
            throw new IllegalArgumentException();
        }

        if (fuel == 'H' && !(letters.contains("HY"))) {
            throw new IllegalArgumentException();
        }
        if (fuel == 'D' || fuel == 'G') {
            if (letters.contains("HY") || letters.contains("EL") || letters.contains("EK")) {

                throw new IllegalArgumentException();
            }

        }

        this.regnr = regnr;

    }

    public String getRegistrationNumber() {

        return regnr;
    }

    public void setVeicleType(char type) {
        if (type != 'C' && type != 'M') {
            throw new IllegalArgumentException();
        }
        this.type = type;

    }

    public char getVehicleType() {

        return type;
    }

    public static void main(String[] args) {
        Vehicle v1 = new Vehicle('C', 'H', "HY12345");

        System.out.println(v1.getRegistrationNumber());

    }
}
