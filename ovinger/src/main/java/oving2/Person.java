package oving2;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

public class Person {

    private String name;
    private String eMail;
    private char gender;
    private Date birthDay;
    private String fornavn;
    private String etternavn;

    public void setName(String name) {

        if (!isWhitespace(name)) {
            throw new IllegalArgumentException();
        }

        String fornavn = name.split(" ")[0];
        String etternavn = name.split(" ")[1];

        if (fornavn.length() <= 2 || etternavn.length() <= 2) {
            throw new IllegalArgumentException();
        }
        if (name.split(" ").length > 2) {
            throw new IllegalArgumentException();
        }

        this.name = fornavn + " " + etternavn;
    }

    private String getFornavn(String name) {

        return name.split(" ")[0];
    }

    private String getEtternavn(String name) {

        return name.split(" ")[1];
    }

    public boolean isWhitespace(String name) {
        Pattern pattern = Pattern.compile("\\s");
        Matcher matcher = pattern.matcher(name);
        boolean found = matcher.find();
        return found;
    }

    public void setEmail(String email) {

        String fornavn = getFornavn(name);
        String etternavn = getEtternavn(name);

        String mailFornavn = email.split(".")[0];
        String mellom = email.split(".")[1];
        String mailEtternavn = mellom.split("@")[0];
        String kode = email.substring(email.length() - 2);

        if (fornavn != mailFornavn && etternavn != mailEtternavn) {
            throw new IllegalArgumentException();
        }
        if (!mellom.contains("@")) {
            throw new IllegalArgumentException();
        }
        if (fornavn != mailFornavn || etternavn != mailEtternavn) {
            throw new IllegalArgumentException();
        }
        if (kode != "no") {
            throw new IllegalArgumentException();

        }

        this.eMail = email;

    }

    public void setBirthday(Date day) {
        Date today = new Date();

        if (day.compareTo(today) > 0) {
            throw new IllegalArgumentException();
        }

        this.birthDay = day;
    }

    public void setGender(char gender) {
        if (gender != 'M' && gender != 'F' && gender != '\0') {
            throw new IllegalArgumentException("Invalid gender selection");
        } else {
            this.gender = gender;
        }
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return eMail;
    }

    public char getGender() {
        return gender;
    }

    public Date getBirthday() {
        return birthDay;
    }

    public static void main(String[] args) {
        Person p1 = new Person();

        p1.setName("Ola Nordmann");
        p1.setEmail("Ola.Nordmann@ntnu.no");

        System.out.println(p1.getName());
    }

}
