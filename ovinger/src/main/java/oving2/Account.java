package oving2;

public class Account {

    private double balance;
    private double interest;

    public Account(double balance, double interest) {
        if (balance < 0 || interest < 0) {
            throw new IllegalArgumentException("Must give positive number");
        } else {
            this.balance = balance;
            this.interest = interest;
        }
    }

    public double getBalance() {
        return balance;
    }

    public double getInterestRate() {
        return interest;
    }

    public void setInterestRate(double interest) {
        if (interest <= 0) {
            throw new IllegalArgumentException("Give a positive number");
        } else {
            this.interest = interest;
        }
    }

    public void deposit(double amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Give a positive number");
        } else {
            this.balance += amount;
        }
    }

    public void withdraw(double amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Give a positive number");
        } else if (this.balance - amount < 0) {
            return;

        } else {
            this.balance -= amount;
        }

    }

    public void addInterest() {
        balance += (balance * interest) / 100;

    }

}

/*
 * #1 Metodene kan sies å være en komplett innkapsling av tilstanden fordi all
 * endring
 * av tilstandene skjer av klassens egne metoder.
 * 
 * 
 * # 2 Denne klassen er tjeneste-orientert, fordi den er fokusert på beregning
 * av data.
 */
