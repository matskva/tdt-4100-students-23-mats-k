package oving4;

import java.util.ArrayList;
import java.util.List;

public class Merchant {
    ArrayList<Item> inventory;
    double balance;

    Merchant(double money) {
        inventory = new ArrayList<>();

        if (money < 0) {
            throw new IllegalArgumentException("Must give merchant more than 0 in money");
        }
        this.balance = money;

    }

    public void obtainItem(Item item) {
        this.inventory.add(item);
        item.changeOwner(this);
    }

    public void removeItem(Item item) {

        ArrayList<Item> newList = new ArrayList<>();

        for (Item items : inventory) {
            if (items != item) {
                newList.add(items);
            }

        }
        this.inventory = newList;

    }

    public double getBalance() {
        return this.balance;
    }

    public ArrayList<Item> getInventory() {
        return this.inventory;
    }

    public void sellItem(Item item, Merchant merchant) {
        double price = item.getPrice();
        Merchant seller = item.getOwner();

        if (!this.inventory.contains(item)) {
            throw new IllegalArgumentException("Seller dont have the item in inventory");
        }
        if (merchant == seller) {
            throw new IllegalArgumentException("Can't sell item to himself");
        }
        if (merchant.balance < price) {
            throw new IllegalArgumentException("Buyer can't afford the item");
        }
        if (item.getOwner() != this) {
            throw new IllegalArgumentException("Cant sell item seller don't own");
        }

        seller.inventory.remove(item);
        seller.balance += price;
        merchant.obtainItem(item);
        merchant.balance -= price;
        item.changeOwner(merchant);

    }

    public void itemSale(double discount, Item item) {

        if (item.getOwner() != this) {
            throw new IllegalArgumentException("Cant sell item seller don't own");
        }

        if (discount > 1 || discount < 0) {
            throw new IllegalArgumentException("Invalid discount given!");
        }
        double newPrice = item.price - (item.price * discount);

        item.price = newPrice;

    }

    public void inventorySale(double discount) {

        for (Item item : inventory) {
            itemSale(discount, item);
        }

    }

    public static void main(String[] args) {
        Merchant seller = new Merchant(100);
        Merchant buyer = new Merchant(100);

        Item sword = new Item("Killer", "Sword", 50);
        Item hammer = new Item("sledge", "hammer", 50);
        Item bow = new Item("shot", "bow", 50);

        seller.obtainItem(sword);
        seller.obtainItem(hammer);
        seller.obtainItem(bow);

        // seller.sellItem(sword, buyer);
        System.out.println(seller.getInventory());

        System.out.println("------- set owner -------");

        buyer.obtainItem(bow);
        System.out.println(buyer == bow.getOwner());

    }

}
