package oving4;

import java.util.ArrayList;

public class Tweet {

    String tweet;
    Tweet retweet;
    Tweet originalTweet;
    TwitterAccount account;
    int retweetcount;

    Tweet(TwitterAccount account, String tweet) {
        this.tweet = tweet;
        this.account = account;
    }

    Tweet(TwitterAccount account, Tweet tweet) {
        if (tweet.getOwner() == account) {
            throw new IllegalArgumentException("Can't retweet your own tweet");
        }

        if (tweet.originalTweet != null) {
            this.originalTweet = tweet.originalTweet;
        }
        this.tweet = tweet.getText();
        this.originalTweet = tweet;
        this.account = account;
        tweet.retweetcount = tweet.retweetcount + 1;

    }

    public String getText() {
        return this.tweet;
    }

    public void setOwner(TwitterAccount account) {
        this.account = account;
    }

    public TwitterAccount getOwner() {
        return this.account;
    }

    public Tweet getOriginalTweet() {
        return originalTweet;

    }

    public int getRetweetCount() {
        return this.retweetcount;
    }

    public static void main(String[] args) {
        TwitterAccount ta1 = new TwitterAccount("Mats");
        TwitterAccount ta2 = new TwitterAccount("Eric");
        TwitterAccount ta3 = new TwitterAccount("Erlend");

        Tweet tweet1 = new Tweet(ta1, "Hei på deg");
        Tweet retweet1 = new Tweet(ta2, tweet1);
        Tweet retweet2 = new Tweet(ta3, retweet1);
        // tweet1.setOwner(ta1);

        System.out.println(tweet1.getText());
        System.out.println(retweet2.getOriginalTweet());
    }

}
