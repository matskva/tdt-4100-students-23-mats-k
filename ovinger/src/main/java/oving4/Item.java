package oving4;

public class Item {

    String name;
    String Item;
    double price;
    Merchant owner;

    Item(String name, String item, double price) {
        if (name.length() < 0) {
            throw new IllegalArgumentException("Please enter a name");
        }
        if (price < 0) {
            throw new IllegalArgumentException("Please enter a price higher than 0");
        }

        this.name = name;
        this.Item = item;
        this.price = price;
        this.owner = null;
    }

    public Merchant getOwner() {
        return this.owner;
    }

    public void changeOwner(Merchant buyer) {

        this.owner = buyer;
    }

    public double getPrice() {
        return this.price;
    }

    public void setprice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "navn: " + name + " type: " + Item + " Price; " + price + " Eier: " + owner;
    }

    public static void main(String[] args) {
        Merchant seller = new Merchant(100);
        Merchant buyer = new Merchant(100);
        Item hammer = new Item("sledge", "hammer", 50);

        hammer.changeOwner(seller);
        System.out.println(hammer.getOwner() == buyer);

    }

}
