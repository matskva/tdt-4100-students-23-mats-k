package oving4;

public class Partner {

    private String name;
    private Partner partner;

    Partner(String name) {
        this.name = name;
        partner = null;

    }

    void setPartner(Partner partner) {
        dissolve();
        this.partner = partner;

        if (partner != null) {
            partner.dissolve();
            partner.partner = this;
        }

    }

    void dissolve() {
        if (partner == null)
            return;

        partner.partner = null;
        partner = null;
    }

    public Partner getPartner() {
        return partner;
    }

    public String getName() {
        return name;
    }


    public static void main(String[] args) {
        Partner p1 = new Partner("Mats");
        System.out.println(p1);
    }

}
