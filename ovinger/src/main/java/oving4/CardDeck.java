package oving4;

import java.util.ArrayList;
import java.util.List;
import java.util.jar.Manifest;

public class CardDeck {

    ArrayList<Card> Deck = new ArrayList<>();

    CardDeck(int n) {

        if (n < 0 || n > 13) {
            throw new IllegalArgumentException();
        }

        for (int i = 1; i < n + 1; i++) {
            Card sparCard = new Card('S', i);

            Deck.add(sparCard);
        }
        for (int i = 1; i < n + 1; i++) {
            Card heartCard = new Card('H', i);

            Deck.add(heartCard);
        }
        for (int i = 1; i < n + 1; i++) {
            Card diamondCard = new Card('D', i);

            Deck.add(diamondCard);
        }
        for (int i = 1; i < n + 1; i++) {
            Card clowerCard = new Card('C', i);

            Deck.add(clowerCard);
        }

    }

    public int getCardCount() {
        return Deck.size();
    }

    public ArrayList<Card> getCardDeck() {
        return Deck;
    }

    public Card getCard(int n) {
        if (n > Deck.size()) {
            throw new IllegalArgumentException("Trying to access a card outside the deck");
        } else {
            return Deck.get(n);
        }

    }

    public ArrayList<Card> shufflePerfectly() {

        List<Card> firstHalf = Deck.subList(0, Deck.size() / 2 + (Deck.size() % 2));
        List<Card> secondHalf = Deck.subList(Deck.size() / 2 + (Deck.size() % 2), Deck.size());

        ArrayList<Card> shuffledCards = new ArrayList<Card>(Deck.size());

        int n = 0;
        int m = 0;

        for (int i = 0; i < Deck.size(); i++) {
            if (i % 2 == 0) {
                shuffledCards.add(i, firstHalf.get(n));
                n++;

            } else {
                shuffledCards.add(i, secondHalf.get(m));
                m++;

            }
        }
        return this.Deck = shuffledCards;
    }

    public void deal(CardHand hand, int n) {
        CardHand ch = new CardHand();

        for (int i = n; i > 0; i--) {
            Card tmp = Deck.get(Deck.size() - 1);
            Deck.remove(Deck.size() - 1);
            hand.addCard(tmp);

        }
    }

    public static void main(String[] args) {
        CardDeck cd = new CardDeck(2);
        CardHand ch = new CardHand();

        System.out.println(cd.getCardDeck());
        cd.deal(ch, 3);
        System.out.println(cd.getCardDeck());
        System.out.println(ch.getCardDeck());

    }
}
