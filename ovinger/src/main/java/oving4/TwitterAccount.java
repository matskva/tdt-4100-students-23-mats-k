package oving4;

import java.util.ArrayList;

public class TwitterAccount {

    String account;
    ArrayList<TwitterAccount> following;
    ArrayList<TwitterAccount> followers;
    int tweetCount;
    int reTweetCount;
    ArrayList<Tweet> tweets = new ArrayList<>();
    ArrayList<Tweet> retweets = new ArrayList<>();

    TwitterAccount(String name) {
        this.account = name;
        following = new ArrayList<>();
        followers = new ArrayList<>();
        ArrayList<Tweet> tweets = new ArrayList<>();
        ArrayList<Tweet> retweets = new ArrayList<>();
    }

    public String getUserName() {
        return this.account;
    }

    public void follow(TwitterAccount account) {
        this.following.add(account);
        account.followers.add(this);
    }

    public void unfollow(TwitterAccount account) {
        ArrayList<TwitterAccount> newfollowing = new ArrayList<>();
        ArrayList<TwitterAccount> newfollowers = new ArrayList<>();

        for (TwitterAccount user : following) {
            if (user != account) {
                newfollowing.add(account);
            }
            if (user != this) {
                newfollowers.add(account);
            }
        }
        this.following = newfollowing;
        account.followers = newfollowers;
    }

    public boolean isFollowing(TwitterAccount account) {
        for (TwitterAccount accounts : following) {
            if (account == accounts) {
                return true;
            }
        }
        return false;
    }

    public boolean isFollowedBy(TwitterAccount account) {

        for (TwitterAccount accounts : this.followers) {
            if (accounts == account) {
                return true;

            }
        }
        return false;
    }

    public void tweet(String tweet) {
        this.tweets.add(0, (new Tweet(this, tweet)));
        this.tweetCount += 1;
    }

    public void retweet(Tweet tweet) {

        TwitterAccount originalTweetOwner = tweet.getOriginalTweet().getOwner();

        if (tweet.getOwner().retweets != null) {
            originalTweetOwner.retweets.add(new Tweet(this, tweet));
        }

        tweet.getOwner().retweets.add(new Tweet(this, tweet));
        this.tweets.add(new Tweet(this, tweet));

    }

    public Tweet getTweet(int i) {
        return tweets.get((i - 1));

    }

    public int getTweetCount() {
        return tweets.size();
    }

    public int getRetweetCount() {
        return retweets.size();

    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return this.account;
    }

    public static void main(String[] args) {

        TwitterAccount mats = new TwitterAccount("Mats");
        TwitterAccount eric = new TwitterAccount("Eric");
        TwitterAccount erlend = new TwitterAccount("Erlend");

        mats.follow(eric);
        eric.tweet("Hallo");
        mats.retweet(eric.getTweet(1));
        erlend.retweet(mats.getTweet(1));

        System.out.println(eric.getRetweetCount());

    }
}
