package oving1;

public class Digit {

    public int numSys;
    int number = 0;

    public Digit(int n) {
        this.numSys = n;
    }

    public int getValue() {
        return number;
    }

    public boolean increment() {

        number++;
        if (number == numSys) {
            number = 0;
            return true;
        } else {

            return false;
        }
    }

    public int getBase() {
        return numSys;
    }

    public String toString() {
        String letters[] = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L",
                "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

        if (number > 9) {
            return letters[number - 10];
        } else {
            return Integer.toString(number);
        }

    }
}
