package oving1;

public class Account {
    double balance = 0.0;
    double interestRate = 0.0;

    public double deposit(double amount) {

        if (amount > 0) {
            balance += amount;
        }

        return balance;

    }

    public double addInterest() {
        double rente = (balance * interestRate) / 100;
        balance += rente;

        return balance;

    }

    public double getBalance() {
        return balance;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public double setInterestRate(double rate) {
        interestRate = rate;

        return interestRate;
    }

    public String toString() {
        return "Your current balance is " + balance + " with an interest rate of " + interestRate;
    }

    public static void main(String[] args) {
        Account account = new Account();
        account.setInterestRate(2);
        account.deposit(200);
        account.addInterest();
        System.out.println(account.toString());
    }
}
