package oving1;

public class StopWatch {

    private int ticks;
    private boolean timer = false;
    private boolean started = false;
    private int lap;
    private int stop;
    private int tick;
    private int falseNumber = -1;

    public boolean isStarted() {
        return timer;

    }

    public boolean isStopped() {
        return timer;

    }

    public int getTicks() {
        return ticks + tick;

    }

    public int getTime() {
        if (!started) {
            return -1;
        }
        return ticks;
    }

    public int getLapTime() {
        if (ticks == 0) {
            return -1;
        }
        return ticks - lap;
    }

    public int getLastLapTime() {
        if (ticks == 0) {
            return -1;
        }
        return lap;
    }

    public void tick(int tick) {
        this.ticks += tick;
    }

    public void start() {
        timer = true;
        started = true;
        while (timer == true) {
            ticks++;
        }
    }

    public void lap() {
        this.lap = ticks;
    }

    public void stop() {
        timer = false;
        stop++;
    }
}
