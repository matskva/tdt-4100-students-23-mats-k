package oving1;

public class Location {
    int x = 0;
    int y = 0;

    public int up() {
        y -= 1;

        return y;
    }

    public int down() {
        y += 1;

        return y;
    }

    public int left() {
        x -= 1;

        return x;
    }

    public int right() {
        x += 1;

        return x;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String toString() {
        return "(" + x + "," + y + ")";
    }
}
