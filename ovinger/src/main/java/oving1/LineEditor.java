package oving1;

public class LineEditor {

    private String text;
    private int insertionIndex;

    public void left() {
        if (insertionIndex == 0) {
            return;
        }
        insertionIndex--;
    }

    LineEditor() {
        text = new String();
    }

    public void right() {
        if (insertionIndex > this.text.length() || this.text.length() == 0) {
            return;
        }
        insertionIndex++;
    }

    void insertString(String s) {
        String rightText = text.substring(0, insertionIndex);
        String leftText = text.substring(insertionIndex);

        text = rightText + s + leftText;
        insertionIndex += s.length();

    }

    public void deleteLeft() {
        StringBuilder newText = new StringBuilder(text);

        try {

            newText.deleteCharAt(insertionIndex - 1);

        } catch (Exception e) {
            return;
        }
        this.text = newText.toString();
        this.left();

    }

    // StringBuilder newText = new StringBuilder(text);

    // newText.deleteCharAt(insertionIndex - 1);

    // this.text = newText.toString();

    // String leftText = this.text.substring(0, insertionIndex - 1);
    // String rightText = this.text.substring(insertionIndex);

    // text = leftText + rightText;
    // }

    public void deleteRight() {

        if (this.insertionIndex == this.text.length()) {
            return;
        }

        StringBuilder newText = new StringBuilder(text);

        newText.deleteCharAt(insertionIndex);

        this.text = newText.toString();
        // String leftText = this.text.substring(0, insertionIndex);
        // String rightText = this.text.substring(insertionIndex + 1);

        // text = leftText + rightText;

    }

    public String getText() {
        return text;
    }

    public void setText(String str) {
        this.text = str;
    }

    public int getInsertionIndex() {
        return insertionIndex;
    }

    public void setInsertionIndex(int i) {
        this.insertionIndex = i;
    }

    public String toString()

    {

        String leftText = this.text.substring(0, insertionIndex);
        String rightText = this.text.substring(insertionIndex);

        return leftText + "|" + rightText;

    }

}
