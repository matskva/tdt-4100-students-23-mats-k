package oving1;

public class UpOrDownCounter {
    int counter;
    int end;

    public UpOrDownCounter(int counter, int end) {
        this.counter = counter;
        this.end = end;

        if (counter == end) {
            throw new IllegalArgumentException("Counter and end can't be the same number!");
        }
    }

    public int getCounter() {
        return counter;
    }

    public boolean count() {
        if (counter + 1 == end) {
            counter++;
            return false;
        }

        else if (counter - 1 == end) {
            counter--;
            return false;
        }

        else if (end > counter) {
            counter++;
            return true;

        } else if (end < counter) {
            counter--;
            return true;
        }
        return false;
    }

    public String toString() {
        return "count: " + counter + " end: " + end;
    }

    public static void main(String[] args) {
        UpOrDownCounter count = new UpOrDownCounter(1, 5);
        System.out.println(count.toString());
        count.count();
        System.out.println(count.toString());
        count.count();
        System.out.println(count.toString());
        count.count();
        System.out.println(count.toString());
        System.out.println(count.count());
        System.out.println(count.toString());
        System.out.println(count.count());
    }

}
