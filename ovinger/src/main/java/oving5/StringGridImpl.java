package oving5;

import java.util.*;

public class StringGridImpl implements StringGrid {

    int rows;
    int column;
    ArrayList<Row> stringGrid;

    StringGridImpl(int rows, int column) {
        this.stringGrid = new ArrayList<>();
        for (int i = 0; i < rows; i++) {
            stringGrid.add(new Row(column));
        }

        this.rows = rows;
        this.column = column;

    }

    @Override
    public int getRowCount() {

        return this.rows;
    }

    @Override
    public int getColumnCount() {
        return this.column;
    }

    @Override
    public String getElement(int row, int column) {

        return stringGrid.get(row).getString(column);
    }

    @Override
    public void setElement(int row, int column, String element) {

        stringGrid.get(row).addString(column, element);

    }

    public void printAsGrid() {

        for (Row row : stringGrid) {
            System.out.println(row);
        }
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return stringGrid.toString();
    }

    public static void main(String[] args) {

        StringGridImpl sg = new StringGridImpl(3, 4);

        sg.setElement(2, 0, "Mats");
        sg.printAsGrid();

    }

}
