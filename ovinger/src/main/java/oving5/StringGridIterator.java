package oving5;

import java.util.Iterator;

public class StringGridIterator implements Iterator<String> {

    boolean rowMajor;
    StringGrid stringGrid;
    int sizeOfGrid;
    int currentSize;

    int rowIndex;
    int columnIndex;

    StringGridIterator(StringGrid grid, boolean bool) {
        this.rowMajor = bool;
        this.stringGrid = grid;
        this.currentSize = 0;
        this.sizeOfGrid = grid.getColumnCount() * grid.getRowCount();
        rowIndex = 0;
        columnIndex = 0;

    }

    @Override
    public boolean hasNext() {

        return currentSize < sizeOfGrid;

    }

    @Override
    public String next() {

        String result;

        if (rowMajor) {

            if (columnIndex == stringGrid.getColumnCount()) {
                columnIndex = 0;
                rowIndex++;
            }

            result = stringGrid.getElement(rowIndex, columnIndex);
            columnIndex++;
            currentSize++;

        }

        else {

            if (rowIndex == stringGrid.getRowCount()) {
                rowIndex = 0;
                columnIndex++;
            }

            result = stringGrid.getElement(rowIndex, columnIndex);
            rowIndex++;
            currentSize++;

        }

        return result;

    }

    public void remove() {
        throw new UnsupportedOperationException("Can't do that here!");
    }

    public static void main(String[] args) {
        StringGridImpl grid = new StringGridImpl(2, 3);

        StringGridIterator it = new StringGridIterator(grid, true);

        grid.setElement(0, 0, "0, 0");
        grid.setElement(0, 1, "0, 1");
        grid.setElement(0, 2, "0, 2");
        grid.setElement(1, 0, "1, 0");
        grid.setElement(1, 1, "1, 1");
        grid.setElement(1, 2, "1, 2");

        grid.printAsGrid();

        System.out.println(it.next());
        System.out.println(it.next());
        System.out.println(it.next());

    }

}
