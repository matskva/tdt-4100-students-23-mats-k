package oving5;

public class Person2 implements Named {

    String fullName;

    public Person2(String fullName) {
        this.fullName = fullName;
    }

    private String[] splitName(String name) {

        return name.split(" ");
    }

    @Override
    public void setGivenName(String name) {

        this.fullName = name + " " + splitName(this.fullName)[1];

    }

    @Override
    public String getGivenName() {

        return splitName(fullName)[0];
    }

    @Override
    public void setFamilyName(String name) {

        this.fullName = splitName(this.fullName)[0] + " " + name;
    }

    @Override
    public String getFamilyName() {
        return splitName(fullName)[1];
    }

    @Override
    public void setFullName(String name) {
        this.fullName = name;

    }

    @Override
    public String getFullName() {

        return this.fullName;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return this.fullName;
    }

    public static void main(String[] args) {
        Person2 p2 = new Person2("Mats Kvanvik");

        System.out.println(p2);

        p2.setGivenName("Sander");
        System.out.println(p2);

        p2.setFamilyName("Zahl");
        System.out.println(p2);
    }

}
