package oving5;

import java.util.ArrayList;

public class Row {

    ArrayList<String> row;
    int maxSize;

    Row(int maxSize) {
        this.row = new ArrayList<>();
        this.maxSize = maxSize;

        for (int i = 0; i < maxSize; i++) {
            row.add("0");
        }

    }

    public String getString(int index) {
        return row.get(index);
    }

    public void addString(String string) {
        this.row.add(string);
    }

    public void addString(int index, String string) {

        if (index > maxSize) {
            throw new IllegalArgumentException("Row too small");
        }

        this.row.remove(index);

        this.row.add(index, string);

    }

    public ArrayList<String> getRow() {
        return this.row;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return row.toString() + '\n';
    }

}
