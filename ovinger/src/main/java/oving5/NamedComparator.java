package oving5;

import java.util.Comparator;

public class NamedComparator implements Comparator<Named> {

    public int compare(Named o1, Named o2) {

        if (o1.getFamilyName().compareTo(o2.getFamilyName()) == 0) {

            if (o1.getGivenName().compareTo(o2.getGivenName()) < 0) {
                return -1;
            } else if (o1.getGivenName().compareTo(o2.getGivenName()) > 0) {
                return 1;
            }
        }

        else if (o1.getFamilyName().compareTo(o2.getFamilyName()) != 0) {
            if (o1.getFamilyName().compareTo(o2.getFamilyName()) < 0) {
                return -1;
            } else if (o1.getFamilyName().compareTo(o2.getFamilyName()) > 0) {
                return 1;
            }
        }

        return 0;

    }

}
