package oving5;

public class Card {
    private char color;
    private int number;

    Card(char color, int number) {
        setColor(color);
        setNumber(number);
    }

    private void setColor(char color) {
        if (color != 'H' && color != 'D' && color != 'S' && color != 'C') {
            throw new IllegalArgumentException("Invalid color");
        } else {
            this.color = color;
        }
    }

    private void setNumber(int number) {
        if (number < 1 || number > 13) {
            throw new IllegalArgumentException("invalid card number value");
        }
        this.number = number;
    }

    public char getSuit() {
        return this.color;
    }

    public int getFace() {
        return this.number;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return color + "" + number;
    }

    public static void main(String[] args) {
        Card c1 = new Card('S', 13);
        System.out.println(c1);

    }
}
