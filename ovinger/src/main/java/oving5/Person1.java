package oving5;

public class Person1 implements Named {

    String givenName;
    String familyName;

    Person1(String given, String family) {
        this.givenName = given;
        this.familyName = family;
    }

    @Override
    public void setGivenName(String name) {
        this.givenName = name;
    }

    @Override
    public String getGivenName() {
        return this.givenName;
    }

    @Override
    public void setFamilyName(String name) {
        this.familyName = name;
    }

    @Override
    public String getFamilyName() {
        return this.familyName;
    }

    @Override
    public void setFullName(String name) {

        String[] fullName = name.split(" ");

        setGivenName(fullName[0]);
        setFamilyName(fullName[1]);

    }

    @Override
    public String getFullName() {

        return this.givenName + " " + this.familyName;
    }

}
