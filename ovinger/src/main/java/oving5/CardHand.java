package oving5;

import java.util.ArrayList;
import java.util.Iterator;

public class CardHand implements CardContainer {
    ArrayList<Card> hand;

    CardHand() {
        this.hand = new ArrayList<Card>();

    }

    public int getCardCount() {
        return hand.size();
    }

    public ArrayList<Card> getCardDeck() {
        return hand;
    }

    public Card getCard(int n) {
        if (n > hand.size()) {
            throw new IllegalArgumentException("Trying to access a card outside the deck");
        } else {
            return hand.get(n);
        }

    }

    public void addCard(Card card) {
        hand.add(card);
    }

    public void play(int n) {
        hand.remove(hand.get(n));
    }

    public Iterator<Card> iterator() {
        return hand.iterator();
    }

    public static void main(String[] args) {
        CardHand ch1 = new CardHand();

        System.out.println(ch1.getCardDeck());
    }

}
