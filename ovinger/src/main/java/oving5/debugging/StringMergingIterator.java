package oving5.debugging;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class StringMergingIterator implements Iterator<String> {

	private Iterator<String> first;
	private Iterator<String> second;
	private boolean turnSwitch;

	public StringMergingIterator(Iterator<String> first, Iterator<String> second) {
		this.first = first;
		this.second = second;
		this.turnSwitch = true;
	}

	@Override
	public boolean hasNext() {
		return first.hasNext() || second.hasNext();
	}

	@Override
	public String next() {

		// if (!(second.hasNext())) {
		// throw new NoSuchElementException();
		// }

		String result = null;

		if (!first.hasNext()) { // Dersom first ikke har flere next må jeg hente second sine next, for
			result = second.hasNext() ? second.next() : null;  
		} else if (!second.hasNext()) {
			result = second.next();
		} else {
			if (turnSwitch) {

				if (!(first.hasNext())) {
					result = second.next();
				} else {
					result = first.next();
				}

				turnSwitch = false;
			} else if (!turnSwitch) {
				result = second.next();
				turnSwitch = true;
			}

		}

		return result;
	}

}
