package oving5;

import java.util.ArrayList;
import java.util.Iterator;

public class CardContainerIterator implements Iterator<Card> {

    public Iterator<Card> it;

    public CardContainerIterator(CardContainer it) {
        this.it = it.getCardDeck().iterator();
    }

    @Override
    public boolean hasNext() {
        return it.hasNext();
    }

    @Override
    public Card next() {
        return it.next();
    }

}
