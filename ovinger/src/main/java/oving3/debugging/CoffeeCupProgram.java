package oving3.debugging;

import java.util.Random;

public class CoffeeCupProgram {

	private CoffeeCup cup;
	private Random r;

	public void init() {
		cup = new CoffeeCup();
		r = new Random(123456789L);
	}

	public void run() {
		// part1();
		part2();
	}

	private void part1() {
		cup.increaseCupSize(40.0);
		cup.fillCoffee(20.5);
		cup.drinkCoffee(Math.floor(r.nextDouble() * 20.5));
		cup.fillCoffee(32.5);
		cup.drinkCoffee(Math.ceil(r.nextDouble() * 38.9));
		cup.drinkCoffee(Math.ceil(r.nextDouble() * 42));
		cup.increaseCupSize(17);
		// cup.drinkCoffee(40); ## currenVolum = 5 og volum på 57, men prøver å drikke
		// 40
		// cup.drinkCoffee(Math.ceil(r.nextDouble() * 42));
		// cup.drinkCoffee(Math.floor(r.nextDouble() * 20.5));
		cup.fillCoffee(32.5);
		cup.drinkCoffee(Math.ceil(r.nextDouble() * 38.9));
		// cup.drinkCoffee(Math.ceil(r.nextDouble() * 42)); ## Currenvolum på 2.5 prøver
		// å drikke 42, volum fortsatt 57.
		cup.increaseCupSize(17);
	}
	// det er canDrink() metoden som utløser unntaket

	private void part2() {
		cup = new CoffeeCup(40.0, 20.5); // Capacity: 40, currentVolume: 20,5
		r = new Random(987654321L);
		cup.drinkCoffee(Math.floor(r.nextDouble() * 20.5)); // Capacity: 40, currentVolume: 14,5
		cup.fillCoffee(Math.floor(r.nextDouble() * 30)); // Capacity: 40, currentVolume: 38,5
		cup.drinkCoffee(Math.ceil(r.nextDouble() * 38.9)); // Capacity: 40, currentVolume: 36,5
		cup.drinkCoffee(Math.ceil(r.nextDouble() * 42)); // Capacity: 40, currentVolume: 6,5
		cup.increaseCupSize(Math.floor(r.nextDouble() * 26)); // Capacity: 40, currentVolume: 6.5
		// cup.fillCoffee(Math.ceil(r.nextDouble() * 59)); // Feil utløst av fillCoffe,
		// fordi den prøver å fylle opp mer
		// enn Capacity, utløser en IllegalArgumentException
		// cup.drinkCoffee(Math.ceil(r.nextDouble() * 42)); // Utløser også
		// IllegalArgumentException da den prøver å drikke mer enn currentvolume
		cup.fillCoffee(Math.floor(r.nextDouble() * 30)); // Capacity: 63, currentVolume: 6,5
		cup.increaseCupSize(Math.floor(r.nextDouble() * 35)); // Capacity: 63, currentVolume: 6,5
		cup.increaseCupSize(Math.floor(r.nextDouble() * 26));// Capacity: 65, currentVolume: 6,5
	}

	public static void main(String[] args) {
		CoffeeCupProgram program = new CoffeeCupProgram();
		program.init();
		program.run();
	}

}
