package oving3;

public class Nim {

    private int[] pileArray = new int[3];

    Nim(
            int pileSize) {
        pileArray[0] = pileSize;
        pileArray[1] = pileSize;
        pileArray[2] = pileSize;
    }

    Nim() {
        pileArray[0] = 10;
        pileArray[1] = 10;
        pileArray[2] = 10;
    }

    public void removePieces(int number, int targetPile) {

        if (isGameOver()) {
            throw new IllegalStateException("The game is over");
        }

        else if (number < 1 || number > pileArray[targetPile]) {
            throw new IllegalArgumentException(
                    "Number must be higher than 1 and cant be larger than the size of the pile");
        }

        else {
            pileArray[targetPile] -= number;
        }

    }

    public boolean isValidMove(int number, int targetPile) {

        if (pileArray[targetPile] - number < 0 || isGameOver() || number < 1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean isGameOver() {
        for (int x : pileArray) {
            if (x == 0) {
                return true;
            }
        }
        return false;
    }

    public int getPile(int targetPile) {
        return pileArray[targetPile];
    }

    public String toString() {

        return "Haug 1: " + pileArray[0] + "haug 2: " + pileArray[1] + "haug 3: " + pileArray[2];

    }

    public static void main(String[] args) {
        Nim nim = new Nim(5);

        nim.removePieces(3, 1);
        System.out.println(nim.getPile(1));
        nim.removePieces(3, 1);
    }
}
