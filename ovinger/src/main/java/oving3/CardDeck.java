package oving3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CardDeck {

    ArrayList<Card> Deck = new ArrayList<Card>();

    CardDeck(int n) {

        if (n < 0 || n > 13) {
            throw new IllegalArgumentException();
        }

        for (int i = 1; i < n + 1; i++) {
            Card sparCard = new Card('S', i);

            Deck.add(sparCard);
        }
        for (int i = 1; i < n + 1; i++) {
            Card heartCard = new Card('H', i);

            Deck.add(heartCard);
        }
        for (int i = 1; i < n + 1; i++) {
            Card diamondCard = new Card('D', i);

            Deck.add(diamondCard);
        }
        for (int i = 1; i < n + 1; i++) {
            Card clowerCard = new Card('C', i);

            Deck.add(clowerCard);
        }

    }

    public int getCardCount() {
        return Deck.size();
    }

    public ArrayList getCardDeck() {
        return Deck;
    }

    public Card getCard(int n) {
        if (n > Deck.size()) {
            throw new IllegalArgumentException("Trying to access a card outside the deck");
        } else {
            return Deck.get(n);
        }

    }

    public ArrayList shufflePerfectly() {

        List<Card> firstHalf = Deck.subList(0, Deck.size() / 2 + (Deck.size() % 2));
        List<Card> secondHalf = Deck.subList(Deck.size() / 2 + (Deck.size() % 2), Deck.size());

        ArrayList<Card> shuffledCards = new ArrayList<Card>(Deck.size());

        int n = 0;
        int m = 0;

        for (int i = 0; i < Deck.size(); i++) {
            if (i % 2 == 0) {
                shuffledCards.add(i, firstHalf.get(n));
                n++;

            } else {
                shuffledCards.add(i, secondHalf.get(m));
                m++;

            }
        }
        return this.Deck = shuffledCards;
    }

    public static void main(String[] args) {
        CardDeck cd1 = new CardDeck(2);
        System.out.println(cd1.getCardDeck());
        System.out.println(cd1.shufflePerfectly());

    }
}
