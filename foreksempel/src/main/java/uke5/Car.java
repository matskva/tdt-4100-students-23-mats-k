package uke5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Car implements Comparable<Car> {
    int seats;
    String plate;

    public Car(int seats, String plate) {
        this.seats = seats;
        this.plate = plate;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return plate + "(" + seats + ")";
    }

    public static void main(String[] args) {
        Car car = new Car(4, "AA41383");
        Car car2 = new Car(2, "XX41383");
        Car car3 = new Car(5, "AB1383");
        Car car4 = new Car(7, "VY41383");

        List<Car> liste = new ArrayList<>();

        liste.add(car);
        liste.add(car2);
        liste.add(car3);
        liste.add(car4);

        System.out.println(liste);

        Collections.sort(liste);
    }

    @Override
    public int compareTo(Car o) {
        // TODO Auto-generated method stub
        return this.plate > o.plate;
    }

}
