package uke12.pizza;

import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class LecturePizzaApp extends Application {

    @Override
    public void start(final Stage primaryStage) throws Exception {

        primaryStage.setTitle("App");
        primaryStage.setScene(new Scene(FXMLLoader.load(getClass().getResource("lecturecontroller/pizza.fxml"))));
        primaryStage.show();
    }

    public static void main(final String[] args) {
        Application.launch(args);
    }
}
