package uke12.pizza;

import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MyPizzaApp extends Application {

    // Trengs ikke lenger.
    // private static final Logger logger = Logger.getLogger(MyPizzaApp.class.getName());

    @Override
    public void start(final Stage primaryStage) throws Exception {

        // To enable use of the logger, we need to add this to module-info.java:
        // requires java.logging; // Gjort det, du skal ha det i orden.

        primaryStage.setTitle("App");
        primaryStage.setScene(new Scene(FXMLLoader.load(getClass().getResource("view/pizza.fxml"))));
        primaryStage.show();
    }

    public static void main(final String[] args) {
        Application.launch(args);
    }
}
