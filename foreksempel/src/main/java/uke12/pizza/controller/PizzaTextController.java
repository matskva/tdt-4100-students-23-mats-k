package uke12.pizza.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import uke12.pizza.model.*;
import uke12.pizza.model.toppingdecorators.Cheese;
import uke12.pizza.model.toppingdecorators.Ham;
import uke12.pizza.model.toppingdecorators.ITopping;
import uke12.pizza.model.toppingdecorators.Mushroom;
import uke12.pizza.model.toppingdecorators.Olives;
import uke12.pizza.model.toppingdecorators.Onions;
import uke12.pizza.model.toppingdecorators.Pepperoni;
import uke12.pizza.model.toppingdecorators.Pineapple;

public class PizzaTextController {
    private PizzaStore store;
    private Scanner scanner;

    public PizzaTextController(PizzaStore store) {
        this.store = store;
        this.scanner = new Scanner(System.in);
    }

    public void run() {
        System.out.println("Welcome to PizzaStore! Here is the syntax for ordering a pizza:");
        System.out.println("  <customer name> <topping1> <topping2> ...");
        System.out.println("Available toppings:");
        List<ITopping> toppings = store.getToppingsList();
        for (ITopping topping : toppings) {
            System.out.println("  " + topping.getName());
        }
        System.out.println("What would you like to do?");

        while (true) {
            System.out.print("list, order name topping topping ..., pickup name: ");
            String input = scanner.nextLine();
            String[] parts = input.split(" ");
            String command = parts[0];

            if (command.equalsIgnoreCase("quit")) {
                break;
            } else if (command.equalsIgnoreCase("list")) {
                System.out.println(store);
            } else if (command.equalsIgnoreCase("pickup")) {
                String customerName = parts[1];
                double cost = store.collectPizzas(customerName);
                System.out.println(customerName + ", your order is ready! The total cost is " + cost + " kr.");
            } else {
                String customerName = command;
                String toppingsList = input.substring(command.length()).trim();
                store.addToOrder(customerName, toppingsList);
                System.out.println("Added pizza to order for " + customerName + ".");
            }
        }

        scanner.close();
    }

    public static void main(String[] args) {
        List<ITopping> availableToppings = new ArrayList<>();
        availableToppings.add(new Cheese(2.5));
        availableToppings.add(new Ham(3.0));
        availableToppings.add(new Mushroom(1.5));
        availableToppings.add(new Olives(1.5));
        availableToppings.add(new Onions(1.0));
        availableToppings.add(new Pepperoni(2.0));
        availableToppings.add(new Pineapple(2.0));
    
        // Create a PizzaStore instance
        PizzaStore pizzaStore = new PizzaStore(availableToppings);
        PizzaTextController controller = new PizzaTextController(pizzaStore);
        controller.run();
    }
}
