package uke12.pizza.controller;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import uke12.pizza.model.*;
import uke12.pizza.model.listeners.CheeseLover;
import uke12.pizza.model.listeners.ProfitListener;
import uke12.pizza.model.toppingdecorators.Cheese;
import uke12.pizza.model.toppingdecorators.Ham;
import uke12.pizza.model.toppingdecorators.ITopping;
import uke12.pizza.model.toppingdecorators.Mushroom;
import uke12.pizza.model.toppingdecorators.Olives;
import uke12.pizza.model.toppingdecorators.Onions;
import uke12.pizza.model.toppingdecorators.Pepperoni;
import uke12.pizza.model.toppingdecorators.Pineapple;
import uke12.pizza.util.DebugTextAreaOutputStream;

public class MyPizzaController {

    PizzaStore pizzaStore;

    // Bruker ikke logger her, fant ut at det var bedre å vise at en tar over System.out. Se initialize()
    // private static final Logger logger = Logger.getLogger(MyPizzaController.class.getName());

    
    @FXML ListView<ITopping> availableToppingsView;
    @FXML TextField customerName;
    @FXML TextField customerPickUpName;
    @FXML Button pizzaOrdered;
    @FXML Button pizzaPickedUp;
    @FXML Label pizzaOrders;
    @FXML TextArea debugTextArea;

    @FXML
    public void initialize() {
        List<ITopping> availableToppings = new ArrayList<>();
        availableToppings.add(new Cheese(2.5));
        availableToppings.add(new Ham(3.0));
        availableToppings.add(new Mushroom(1.5));
        availableToppings.add(new Olives(1.5));
        availableToppings.add(new Onions(1.0));
        availableToppings.add(new Pepperoni(2.0));
        availableToppings.add(new Pineapple(2.0));

        // create the debugTextArea where system.out is redirected:
        DebugTextAreaOutputStream outputStream = new DebugTextAreaOutputStream(debugTextArea);
        PrintStream printStream = new PrintStream(outputStream);
        // Kidnappe så System.out/err kjøres via printStream i stedet, som igjen skriver til et tekstfelt.
        System.setOut(printStream);
        System.setErr(printStream);

        
        // Create a PizzaStore instance
        pizzaStore = new PizzaStore(availableToppings);

        // Listeners
        CheeseLover cheeseLover = new CheeseLover();
        ProfitListener profitListener = new ProfitListener();
        pizzaStore.addListener(cheeseLover);
        pizzaStore.addListener(profitListener);
        System.out.println("Added listeners");

        // Set the selection mode to MULTIPLE (Ctrl-click to select multiple)
        availableToppingsView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        // Add all toppings from the PizzaStore into the ListView:
        availableToppingsView.getItems().addAll(pizzaStore.getToppingsList());

        // After initalization, update GUI to make sure everything is in order.
        updateGUI();
    }


    @FXML
    void onPizzaOrdered() {
        // First, ListView allows us to get a 'list, of sorts', of the chosen items.
        ObservableList<ITopping> selected = availableToppingsView.getSelectionModel().getSelectedItems();
        pizzaStore.addToOrder(customerName.getText(), pizzaStore.createPizza(selected));
        updateGUI();
    }

    @FXML
    void onPizzaPickedUp() {
        pizzaStore.collectPizzas(customerPickUpName.getText());
        updateGUI();
    }


    private void updateGUI() {
        pizzaOrders.setText(pizzaStore.toString());
        availableToppingsView.getSelectionModel().clearSelection();
        System.out.println("Num customers: "+pizzaStore.getNumberOfCustomers());

        // Avgjøre om knappen for å hente er tilgjengelig eller ikke.
        if (pizzaStore.getNumberOfCustomers() == 0) {
            pizzaPickedUp.setDisable(true);
        }
        else {
            pizzaPickedUp.setDisable(false);
        }
    }

}
