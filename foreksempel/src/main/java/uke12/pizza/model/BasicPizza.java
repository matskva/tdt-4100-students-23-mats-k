package uke12.pizza.model;

import java.util.ArrayList;
import java.util.Collection;

import uke12.pizza.model.toppingdecorators.ITopping;

public class BasicPizza implements Pizza {
    private Collection<ITopping> toppings = new ArrayList<>();

    public String getDescription() {
        return "Basic Pizza";
    }

    public double getCost() {
        return 4.00;
    }

    /* Dette er ChatGPT sin forklaring av hva addTopping gjør:
     * The addTopping method takes a Topping object and adds it to the pizza's list of toppings. 
     * It then returns a new instance of the ToppingDecorator class that wraps the original pizza.
     * 
     * The ToppingDecorator is a special type of class that "decorates" or adds functionality to 
     * an existing object without modifying its underlying structure. It takes in a Pizza object 
     * in its constructor, and it also has its own implementation of the getDescription, getCost, 
     * and getToppings methods.
     * 
     * In this case, the ToppingDecorator adds the description and cost of the new Topping object
     * to the original pizza's description and cost. The getDescription method returns a string 
     * that combines the original pizza's description with the name of the new topping, 
     * separated by a comma. The getCost method returns the original pizza's cost plus the cost 
     * of the new topping. Finally, the getToppings method returns a collection of toppings that 
     * includes both the original pizza's toppings and the new topping.
     * 
     * By returning a new instance of the ToppingDecorator class, the addTopping method ensures 
     * that the original pizza object is not modified. Instead, the new instance of the 
     * ToppingDecorator class represents the modified pizza with the new topping.
     */
    public Pizza addTopping(ITopping topping) {
        toppings.add(topping);

        return new ToppingDecorator(this) {
            public String getDescription() {
                return pizza.getDescription() + ", " + topping.getName();
            }

            public double getCost() {
                return pizza.getCost() + topping.getCost();
            }

            public Collection<ITopping> getToppings() {
                return toppings;
            }
        };
    }

    public Collection<ITopping> getToppings() {
        return toppings;
    }
}
