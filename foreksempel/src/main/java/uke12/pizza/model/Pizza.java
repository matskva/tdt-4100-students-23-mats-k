package uke12.pizza.model;

import java.util.Collection;

import uke12.pizza.model.toppingdecorators.ITopping;

public interface Pizza {
    public String getDescription();
    public double getCost();
    public Pizza addTopping(ITopping topping);
    public Collection<ITopping> getToppings();

    /*
     * hasTopping returnerer om pizzaen har en topping lik parameteren.
     * Den sjekker kun om klassen er lik, ikke om prisen satt er lik. 
     */
    default boolean hasTopping(ITopping topping) {
        Collection<ITopping> toppings = getToppings();
        System.out.println("toppings: "+toppings);
        if (toppings == null || toppings.isEmpty()) {
            return false;
        }
        for (ITopping t : toppings) {
            if (t.getClass() == topping.getClass()) {
                System.out.println("Matching topping");
                return true;
            }
        }
        return false;
    }
}

