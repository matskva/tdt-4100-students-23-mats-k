package uke13;

public class B extends A{
    
    int age;

    public B(String name, int age) {
        super(name);
        this.age = age;
    }

    @Override
    public String toString() {
        return "B [age=" + age + "]";
    }
    
    @Override
    String getRepresentation() {
        return super.getRepresentation() + ":"+age;
    }

    public static void main(String[] args) {
        A a = new A("Ost");
        System.out.println(a);

        a = new B("margarin", 23);
        System.out.println(a);
    }
}
