package uke13.pizza.model.toppingdecorators;


/*
 * 
 * Denne abstrakte klassen har jeg laget slik at den egentlig skal kunne ligge mellom ITopping
 * og selve toppings som cheese og ham. På denne måten kan jeg implementere metoder som equals
 * generelt for alle klasse som arver denne. Jeg har ikke implementert denne løsningen, men
 * det kunne jo vært en spennende ting å gjøre... neste år!
 */

public abstract class UbruktAbstractTopping implements ITopping {

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ITopping)) {
            return false;
        }
        ITopping other = (ITopping) obj;
        return this.getName().equals(other.getName()) && this.getCost() == other.getCost();
    }
    
    
}
