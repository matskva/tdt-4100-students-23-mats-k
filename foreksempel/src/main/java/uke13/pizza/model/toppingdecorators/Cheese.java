package uke13.pizza.model.toppingdecorators;

public class Cheese implements ITopping {
    private double cost;

    public Cheese(double cost) {
        this.cost = cost;
    }

    public String getName() {
        return "Cheese";
    }

    public double getCost() {
        return cost;
    }
}
