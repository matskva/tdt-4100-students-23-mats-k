package uke13.pizza.model.toppingdecorators;

public class Onions implements ITopping {
    private double cost;

    public Onions(double cost) {
        this.cost = cost;
    }

    public String getName() {
        return "Onions";
    }

    public double getCost() {
        return cost;
    }
}
