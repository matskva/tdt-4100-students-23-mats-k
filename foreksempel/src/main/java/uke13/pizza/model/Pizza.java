package uke13.pizza.model;

import java.util.Collection;

import uke13.pizza.model.toppingdecorators.Cheese;
import uke13.pizza.model.toppingdecorators.Ham;
import uke13.pizza.model.toppingdecorators.ITopping;
import uke13.pizza.util.PizzaCosts;

public interface Pizza {
    public String getDescription();
    public double getCost();
    public Pizza addTopping(ITopping topping);
    public Collection<ITopping> getToppings();

    /*
     * hasTopping returnerer om pizzaen har en topping lik parameteren.
     * Den sjekker kun om klassen er lik, ikke om prisen satt er lik. 
     * Default-metoder er ikke pensum.
     */
    default boolean hasTopping(ITopping topping) {
        Collection<ITopping> toppings = getToppings();
        System.out.println("toppings: "+toppings);
        if (toppings == null || toppings.isEmpty()) {
            return false;
        }
        for (ITopping t : toppings) {
            if (t.getClass() == topping.getClass()) {
                System.out.println("Matching topping");
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Pizza pizza = new BasicPizza();
        pizza = pizza.addTopping(new Cheese(PizzaCosts.CHEESE));
        pizza = pizza.addTopping(new Ham(PizzaCosts.HAM));
    }
}

