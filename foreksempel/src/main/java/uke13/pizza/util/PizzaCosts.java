package uke13.pizza.util;

public class PizzaCosts {
    public static final double BasicPizza = 4;
    public static final double CHEESE = 2.5;
    public static final double HAM = 3.0;
    public static final double MUSHROOM = 1.5;
    public static final double OLIVES = 1.5;
    public static final double ONIONS = 1.0;
    public static final double PEPPERONI = 2.0;
    public static final double PINEAPPLE = 2.0;
}
