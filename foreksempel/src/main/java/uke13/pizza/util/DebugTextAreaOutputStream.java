package uke13.pizza.util;

import java.io.IOException;
import java.io.OutputStream;

import javafx.application.Platform;
import javafx.scene.control.TextArea;


/*
 * Denne klassen er en hjelpeklasse som brukes for å kidnappe System.out og sende beskjeder
 * som skrives dit til et JavaFX TextArea i stedet. Dette bruker jeg for å kunne se at
 * utskrifter som gjøres som en del av modellen PizzaStore faktisk vises når programmet
 * kjøres som en app. Klassen vil kun brukes når man kjører det som en app. 
 * 
 * Platform.runLater er noe som gjerne må implementeres når man skal gjøre slikt, når ting
 * kjøres som en app er det alltid mye mer trøblete med 'tråder' og potensielle konflikter.
 */
public class DebugTextAreaOutputStream extends OutputStream {
    private TextArea textArea;

    public DebugTextAreaOutputStream(TextArea textArea) {
        this.textArea = textArea;
    }

    @Override
    public void write(int b) throws IOException {
        Platform.runLater(() -> textArea.appendText(String.valueOf((char) b)));
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        String text = new String(b, off, len);
        Platform.runLater(() -> textArea.appendText(text));
    }

    @Override
    public void write(byte[] b) throws IOException {
        write(b, 0, b.length);
    }
}
