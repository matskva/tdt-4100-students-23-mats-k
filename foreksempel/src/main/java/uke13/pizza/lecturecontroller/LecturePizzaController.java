package uke13.pizza.lecturecontroller;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import uke13.pizza.model.BasicPizza;
import uke13.pizza.model.Pizza;
import uke13.pizza.model.PizzaStore;
import uke13.pizza.model.listeners.CheeseLover;
import uke13.pizza.model.listeners.ProfitListener;
import uke13.pizza.model.toppingdecorators.Cheese;
import uke13.pizza.model.toppingdecorators.Ham;
import uke13.pizza.model.toppingdecorators.ITopping;
import uke13.pizza.model.toppingdecorators.Mushroom;
import uke13.pizza.model.toppingdecorators.Olives;
import uke13.pizza.model.toppingdecorators.Onions;
import uke13.pizza.model.toppingdecorators.Pepperoni;
import uke13.pizza.model.toppingdecorators.Pineapple;
import uke13.pizza.util.DebugTextAreaOutputStream;

public class LecturePizzaController {
    
    PizzaStore pizzaStore;

    // @FXML ListView<ITopping> availableToppingsView;
    // @FXML TextField customerName;
    // @FXML TextField customerPickUpName;
    // @FXML Button pizzaOrdered;
    // @FXML Button pizzaPickedUp;
    // @FXML Label pizzaOrders;
    // @FXML TextArea debugTextArea;

    @FXML ListView<ITopping> availableToppingsView;
    @FXML TextField customerName;
    @FXML TextField customerPickUpName;
    @FXML Button pizzaOrdered;
    @FXML Button pizzaPickedUp;
    @FXML Label pizzaOrders;
    @FXML TextArea debugTextArea;

/*

 */

    @FXML
    public void initialize() {
        List<ITopping> availableToppings = new ArrayList<>();
        availableToppings.add(new Cheese(2.5));
        availableToppings.add(new Ham(3.0));
        availableToppings.add(new Mushroom(1.5));
        availableToppings.add(new Olives(1.5));
        availableToppings.add(new Onions(1.0));
        availableToppings.add(new Pepperoni(2.0));
        availableToppings.add(new Pineapple(2.0));

        // create the debugTextArea where system.out is redirected:
        DebugTextAreaOutputStream outputStream = new DebugTextAreaOutputStream(debugTextArea);
        PrintStream printStream = new PrintStream(outputStream);
        // Kidnappe så System.out/err kjøres via printStream i stedet, som igjen skriver til et tekstfelt.
        System.setOut(printStream);
        System.setErr(printStream);

        
        // Create a PizzaStore instance
        pizzaStore = new PizzaStore(availableToppings);

        // Listeners
        CheeseLover cheeseLover = new CheeseLover();
        ProfitListener profitListener = new ProfitListener();
        pizzaStore.addListener(cheeseLover);
        pizzaStore.addListener(profitListener);
        System.out.println("Added listeners");


        availableToppingsView.getItems().addAll(pizzaStore.getToppingsList());
        // Set the selection mode to MULTIPLE (Ctrl-click to select multiple)
        availableToppingsView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        // Add all toppings from the PizzaStore into the ListView:
        // availableToppingsView.getItems().addAll(pizzaStore.getToppingsList());

        // After initalization, update GUI to make sure everything is in order.
        updateGUI();


        // Et par metoder for å lytte på hendelser i et element.
        // Første reagerer på linjeskift
        customerPickUpName.setOnAction(event -> onPizzaPickup());

        // // Denne reagerer på alle tastetrykk, men vi filtrerer på linjeskift.
        customerName.setOnKeyPressed(event -> {
            System.out.println(event.getCode());
            if(event.getCode() == KeyCode.ENTER){
              onPizzaOrdered();
            }
         }); 

    }

    @FXML
    public void onPizzaOrdered() {
        ObservableList<ITopping> valgt = availableToppingsView.getSelectionModel().getSelectedItems();
        valgt.forEach(System.out::println);
        Pizza pizza = pizzaStore.createPizza(valgt);
        pizzaStore.addToOrder(customerName.getText(), pizza);
        availableToppingsView.getSelectionModel().clearSelection();
        updateGUI();
    }

    @FXML
    public void onPizzaPickup() {
        pizzaStore.collectPizzas(customerPickUpName.getText());
        updateGUI();
    }

    private void updateGUI() {
        pizzaOrders.setText(pizzaStore.toString());

        if (pizzaStore.getNumberOfCustomers() == 0) {
            pizzaPickedUp.setDisable(true);
        }
        else {
            pizzaPickedUp.setDisable(false);
        }
    }
}
