package uke13.pizza.lecturecontroller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import uke13.pizza.model.PizzaStore;
import uke13.pizza.model.toppingdecorators.ITopping;

public class LecturePizzaController {
    
    PizzaStore pizzaStore;

    // @FXML ListView<ITopping> availableToppingsView;
    // @FXML TextField customerName;
    // @FXML TextField customerPickUpName;
    // @FXML Button pizzaOrdered;
    // @FXML Button pizzaPickedUp;
    // @FXML Label pizzaOrders;
    // @FXML TextArea debugTextArea;

    @FXML ListView<ITopping> availableToppingsView;
    @FXML TextField customerName;
    @FXML TextField customerPickUpName;
    @FXML Button pizzaOrdered;
    @FXML Button pizzaPickedUp;
    @FXML Label pizzaOrders;
    @FXML TextArea debugTextArea;

/*
        List<ITopping> availableToppings = new ArrayList<>();
        availableToppings.add(new Cheese(2.5));
        availableToppings.add(new Ham(3.0));
        availableToppings.add(new Mushroom(1.5));
        availableToppings.add(new Olives(1.5));
        availableToppings.add(new Onions(1.0));
        availableToppings.add(new Pepperoni(2.0));
        availableToppings.add(new Pineapple(2.0));

        // create the debugTextArea where system.out is redirected:
        DebugTextAreaOutputStream outputStream = new DebugTextAreaOutputStream(debugTextArea);
        PrintStream printStream = new PrintStream(outputStream);
        // Kidnappe så System.out/err kjøres via printStream i stedet, som igjen skriver til et tekstfelt.
        System.setOut(printStream);
        System.setErr(printStream);

        
        // Create a PizzaStore instance
        pizzaStore = new PizzaStore(availableToppings);

        // Listeners
        CheeseLover cheeseLover = new CheeseLover();
        ProfitListener profitListener = new ProfitListener();
        pizzaStore.addListener(cheeseLover);
        pizzaStore.addListener(profitListener);
        System.out.println("Added listeners");

        // Set the selection mode to MULTIPLE (Ctrl-click to select multiple)
        availableToppingsView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        // Add all toppings from the PizzaStore into the ListView:
        availableToppingsView.getItems().addAll(pizzaStore.getToppingsList());

        // After initalization, update GUI to make sure everything is in order.
        updateGUI();

 */

    @FXML
    public void initialize() {
        
    }

    @FXML
    public void onPizzaOrdered() {
        
    }

    @FXML
    public void onPizzaPickup() {
        
    }

    private void updateGUI() {
        
    }
}
