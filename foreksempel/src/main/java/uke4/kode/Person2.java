package uke4.kode;

public class Person2 {

    private String fullName;

    public Person2(String firstName, String Lastname) {

        setFullName(fullName);

    }
    //

    public String getFirstName() {
        return fullName.split(" ")[0];
    }

    public String getLastName() {
        return fullName.split(" ")[1];
    }

    public String getFullName() {
        return this.fullName;
    }

    public void setFirstName(String firstName) {

        String etternavn = fullName.split(" ")[1];

        this.fullName = firstName + " " + etternavn;

    }

    public void setLastName(String lastName) {
        String fornavn = fullName.split(" ")[0];

        this.fullName = fornavn + " " + lastName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return getFullName();
    }

    public static void main(String[] args) {
        Person2 p2 = new Person2("Ola Nordmann");

        System.out.println(p2);

    }
}
