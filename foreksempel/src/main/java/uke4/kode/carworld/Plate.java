package uke4.kode.carworld;

public class Plate {

    private String regNr;

    public Plate(String regNr) {
        // this.regNr = regNr;

        if (Plate.isValid(regNr)) {
            this.regNr = regNr;
        } else {
            throw new IllegalArgumentException("ugyldig registreringsnummer");
        }
    }

    public static boolean isValid(String regNr) {

        if (regNr.length() != 7) {
            return false;
        }

        // nå vet vi at lengden er 7

        for (int i = 0; i < regNr.length(); i++) {
            char c = regNr.charAt(i);

            if (i < 2) {
                if (!Character.isAlphabetic(c)) {
                    return false;
                }
            } else {
                if (!Character.isDigit(c)) {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return this.regNr;
    }

    public static void main(String[] args) {
        Plate p = new Plate("AB12345");

        System.out.println(p);
    }
}
