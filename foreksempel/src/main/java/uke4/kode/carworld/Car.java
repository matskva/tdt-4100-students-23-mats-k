package uke4.kode.carworld;

import java.util.ArrayList;
import java.util.Collection;

public class Car {

    private Plate plate;

    private Person driver;

    private Collection<Person> passengers = new ArrayList<>();

    private int seats;

    public Car(String regNr, Person d, int seats) {
        this.driver = d;
        this.plate = new Plate(regNr);
        this.passengers = new ArrayList<Person>();
        setSeats(seats);

    }

    public void setplate(String regNr) {
        if (Plate.isValid(regNr)) {
            this.plate = regNr;
        } else {
            throw new IllegalArgumentException("Ugyldig registreringsnummer");
        }
    }

    public void setSeats(int seats) {
        if (seats < 1) {
            throw new IllegalArgumentException("Kan ikke ha mindre enn 1 sete!");
        }
        this.seats = seats;
    }

    public void addPassenger(Person p) {

        int passasjerer = this.passengers.size();

        if (this.seats - passasjerer <= 1) {
            throw new IllegalArgumentException("Har ikke flere seter i bilen!");
        }
        this.passengers.add(p);
    }

    public void removePassenger(Person p) {
        this.passengers.remove(p);
    }

    public void setDriver(Person p) {
        if (p.getAge() >= 18 && p.getLicence()) {
            this.driver = p;
        } else {
            throw new IllegalArgumentException("Ikke gammel nok driver")
        }
    }

    @Override
    public String toString() {
        return "Car [plate=" + plate + ", driver=" + driver + ", passengers=" + passengers + ", seats=" + seats + "]";
    }

    public static void main(String[] args) {
        Person d = new Person("Jostein", 18, true);
        Person b = new Person("Børge");
        Car c1 = new Car("AB12345", d, 3);

        c1.addPassenger(b);
        c1.addPassenger(b);

    }

}
