package uke4.kode.carworld;

public class Person {

    private String name;
    private int age;
    private boolean license;

    public Person(String name, int age, boolean license) {
        this.name = name;
        this.setAge(age);
        this.license = license;
    }

    public Person(String name) {

        this(name, 42, false);

    }

    public Person() {
        this("Børge");
    }

    public void setAge(int age) {
        if (age < 0) {
            throw new IllegalArgumentException("Enter a valid age");
        }
        this.age = age;
    }

    public boolean getLicence() {
        return this.licence;
    }

    public void misteLappen() {
        this.license = false;
    }

    public void passDrivingtest() {
        this.license = true;
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }

    @Override
    public String toString() {
        return "Person [name=" + name + ", age=" + age + ", license=" + license + "]";
    }

    public static void main(String[] args) {
        Person p1 = new Person();

        System.out.println(p1);

        p1.passDrivingtest();
        System.out.println(p1);
        p1.setAge(-1);
    }

}
