package of5_2.kode;

import java.util.Iterator;
import java.util.function.Predicate;

public class FilterAnimalsIterator implements Iterator<Animal> {

    private int pos;
    private Farm farm;
    private Predicate<Animal> predicate;

    public FilterAnimalsIterator(Farm farm, Predicate<Animal> predicate) {
        this.farm = farm;
        this.pos = this.nextLegalIndex(0);
        this.predicate = predicate;

    }

    private int nextLegalIndex(int j) {

        for (int i = j; i < this.farm.getAnimals().size(); i++) {

            Animal currentAnimal = this.farm.getAnimals().get(i);

            if (predicate.test(currentAnimal)) {
                return i;
            }

        }

        return this.farm.getAnimals().size();
    }

    @Override
    public boolean hasNext() {
        int nextIndex = this.nextLegalIndex(pos);

        return nextIndex < this.farm.getAnimals().size();
    }

    @Override
    public Animal next() {

        Animal animal = this.farm.getAnimals().get(this.pos);

        this.pos = this.nextLegalIndex(pos + 1);

        return animal;
    }
}
