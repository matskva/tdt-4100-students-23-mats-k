package of7.lf;

import javafx.scene.paint.Color;

public class Square extends Rectangle {

    public Square(Color color, double sideLength) {
        super(color, sideLength, sideLength);
    }

    @Override
    public String toString() {
        return "Kvadrat - Areal: " + this.getArea();
    }

    public static void main(String[] args) {
        Shape s = new Square(Color.ALICEBLUE, 30);
        System.out.println(s);
    }
    
}
