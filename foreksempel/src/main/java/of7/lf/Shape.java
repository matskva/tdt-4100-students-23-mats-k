package of7.lf;

import javafx.scene.paint.Color;

public abstract class Shape implements Comparable<Shape> {

    private Color color;

    public Shape(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public abstract double getArea();

    @Override
    public String toString() {
        return "Area: " + this.getArea();
    }

    @Override
    public int compareTo(Shape other) {
        // Største skal komme først i listen
        return (int) (other.getArea() - this.getArea());
    }

    public static void main(String[] args) {
    }

}
