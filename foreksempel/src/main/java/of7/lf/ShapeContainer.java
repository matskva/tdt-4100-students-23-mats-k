package of7.lf;

import java.util.ArrayList;
import java.util.List;

public class ShapeContainer {

    private List<Shape> myShapes;

    private int capacity;

    public ShapeContainer(int capacity) {
        this.myShapes = new ArrayList<>();
        this.capacity = capacity;
    }

    public void addShape(Shape shape) {
        if (this.getTotalArea() + shape.getArea() > this.capacity){
            throw new OverFullException();
        }
        myShapes.add(shape);

    }

    public double getTotalArea(){

        // double result = 0;

        // for (Shape s: this.myShapes){
        //     result += s.getArea();
        // }
        // return result;

        return this.myShapes.stream().mapToDouble(s -> s.getArea()).sum();

    }

    public List<Shape> getSortedShapes(){
        // kopier liste for å ikke endre på original-listen
        List<Shape> nyListe = new ArrayList<>(myShapes);
        
        nyListe.sort(null); // inplace sortering av nyListe
        return nyListe;
    }

    public static void main(String[] args) {

        Shape s1 = new Rectangle(null, 40, 60);
        Shape s2 = new Rectangle(null, 40, 50);
        Shape s3 = new Circle(null, 30);
        Shape s4 = new Square(null, 60);

        ShapeContainer shapeContainer = new ShapeContainer(10000);

        try {
            
            shapeContainer.addShape(s1);
            shapeContainer.addShape(s2);
            shapeContainer.addShape(s3);
            shapeContainer.addShape(s4);
        } catch (OverFullException e) {
            System.out.println("Det skjedde noe galt, feilmedling var: " + e.getMessage());
        }

        System.out.println(shapeContainer.getSortedShapes());
    }


}
