package of7.lf;

public class Student extends Person {

    private int studentId;

    public Student(String name, int age, int studentId) {
        super(name, age);
        this.studentId = studentId;
    }

    @Override
    public int getAge() {
        // Eksempel på overriding
        return super.getAge() - 2;
    }

    @Override
    public String toString() {
        return super.toString() + " StudentId=" + this.studentId;
    }

    public static void main(String[] args) {

        Person p1 = new Person("Eirik", 23);

        Student s1 = new Student("Jostein", 24, 57282982);

        Person p2 = new Student("Mathea", 25, 12383987);

        System.out.println(p1);
        System.out.println(s1);
        System.out.println(p2);

    }

}
