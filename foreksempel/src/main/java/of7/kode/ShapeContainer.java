package of7.kode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ShapeContainer {

    private List<Shape> container;
    private int capasity;

    public ShapeContainer(int cap) {
        this.container = new ArrayList<>();
        this.capasity = cap;
    }

    public void addShape(Shape shape) {

        if (this.getTotalArea() + shape.getArea() > capasity) {
            throw new overFullException();
        }

        this.container.add(shape);
    }

    public double getTotalArea() {

        return this.container.stream()
                .mapToDouble(l -> l.getArea()).sum();
        /*
         * mapToDouble(Shape::getArea); gjør det samme som over
         */
    }

    public List<Shape> getSortedList() {

        List<Shape> nyList = new ArrayList<>(container);

        nyList.sort(null);

        return nyList;
    }

}
