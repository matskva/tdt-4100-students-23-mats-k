package of7.kode;

import javafx.scene.paint.Color;

public class Square extends Rectangle {

    public Square(Color color, double sideLength) {
        super(color, sideLength, sideLength);

    }

    public static void main(String[] args) {
        Shape s = new Square(Color.WHITE, 5);

        System.out.println(s.getArea());
    }
}
