package of7.kode;

public class Student extends person {

    int studentid;

    public Student(String name, int age, int studentid) {
        super(name, age);
        this.studentid = studentid;

    }

    @Override
    public String toString() {
        return "Student [studentid=" + studentid + "]";
    }

    // @Override
    // public int getAge() {
    // return super.getAge() - 2;
    // }

    public static void main(String[] args) {
        person p1 = new person("Mats", 20);

        Student s1 = new Student("Morten", 24, 5678);

        person p2 = new Student("Sondre", 21, 80065);

        System.out.println(p1);
        System.out.println(s1);
        System.out.println(p2);
    }
}
