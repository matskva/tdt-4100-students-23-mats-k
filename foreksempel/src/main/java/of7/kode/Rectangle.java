package of7.kode;

import javafx.scene.paint.Color;

public class Rectangle extends Shape {

    public Rectangle(Color color, double length, double height) {
        super(color);
        this.height = height;
        this.length = length;
    }

    private double length;

    private double height;

    @Override
    public double getArea() {
        return length * height;
    }

}
