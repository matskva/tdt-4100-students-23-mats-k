package of7.kode;

import javafx.scene.paint.Color;

public class Car extends Vehicle {

    int dører;

    public Car(String bilmerke, String model, int registreringsår, Color farge, int dører) {
        super(bilmerke, model, registreringsår, farge);
        this.dører = dører;
    }

    @Override
    public String toString() {
        return super.toString() + "Car [dører=" + dører + "]";
    }

    public static void main(String[] args) {
        Vehicle car1 = new Vehicle("Red Bull", "Racerbil", 2023, Color.HOTPINK);
        Vehicle truck = new Truck("Volvo", "blastebil", 1999, Color.BLACK, 233);

        System.out.println(car1);
        System.out.println(truck);
    }

}
