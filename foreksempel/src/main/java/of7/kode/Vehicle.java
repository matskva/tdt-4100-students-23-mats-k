package of7.kode;

import javafx.scene.paint.Color;

public class Vehicle {

    private String bilmerke;

    private String model;

    private int registreringsår;

    private Color farge;

    public Vehicle(String bilmerke, String model, int registreringsår, Color farge) {
        this.bilmerke = bilmerke;
        this.model = model;
        this.registreringsår = registreringsår;
        this.farge = farge;
    }

    public String getBilmerke() {
        return bilmerke;
    }

    public void setBilmerke(String bilmerke) {
        this.bilmerke = bilmerke;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getRegistreringsår() {
        return registreringsår;
    }

    public void setRegistreringsår(int registreringsår) {
        this.registreringsår = registreringsår;
    }

    public Color getFarge() {
        return farge;
    }

    public void setFarge(Color farge) {
        this.farge = farge;
    }

    @Override
    public String toString() {
        return "Vehicle [bilmerke=" + bilmerke + ", model=" + model + ", registreringsår=" + registreringsår
                + ", farge=" + farge + "]";
    }

}
