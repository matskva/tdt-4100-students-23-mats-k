package of7.kode;

import javafx.scene.paint.Color;

public class Truck extends Vehicle {

    private int lasteKapasitet;

    public Truck(String bilmerke, String model, int registreringsår, Color farge, int lasteKapasitet) {
        super(bilmerke, model, registreringsår, farge);
        this.lasteKapasitet = lasteKapasitet;

    }

    @Override
    public String toString() {
        return super.toString() + "Truck [lasteKapasitet=" + lasteKapasitet + "]";
    }

}
