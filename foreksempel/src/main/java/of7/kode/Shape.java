package of7.kode;

import javafx.scene.paint.Color;

public abstract class Shape implements Comparable<Shape> {

    private Color color;

    public Shape(Color color) {
        this.color = color;
    }

    public abstract double getArea();

    @Override
    public int compareTo(Shape other) {

        return (int) (other.getArea() - this.getArea());
    }
}
