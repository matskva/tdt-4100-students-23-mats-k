package uke7.comparator;

import java.util.Comparator;

public class EnkelPersonNavnLengdeComparator implements Comparator<EnkelPerson> {

    @Override
    public int compare(EnkelPerson o1, EnkelPerson o2) {
        // TODO Auto-generated method stub
        return o1.getNavn().length() - o2.getNavn().length();
    }

}
