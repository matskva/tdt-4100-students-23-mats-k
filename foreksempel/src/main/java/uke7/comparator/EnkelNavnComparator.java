package uke7.comparator;

import java.util.Comparator;

public class EnkelNavnComparator implements Comparator<EnkelPerson> {

    public int compare(EnkelPerson o1, EnkelPerson o2) {
        return o1.getNavn().compareTo(o2.getNavn());
    }
}
