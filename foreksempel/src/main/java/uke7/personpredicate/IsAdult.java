package uke7.personpredicate;

import java.util.function.Predicate;

public class IsAdult implements Predicate<Person> {

    @Override
    public boolean test(Person t) {
        // TODO Auto-generated method stub
        return t.getAge() > 17;
    }

}
