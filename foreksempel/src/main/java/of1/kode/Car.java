package of1.kode;

public class Car {

    String brand;
    String model;
    int year;
    int kmDriven;
    double speed;

    public Car(String brand, String model, int year, int kmDriven) {
        this.brand = brand;
        this.model = model;
        this.year = Math.max(1950, year); // limits what the values we can input are
        this.kmDriven = Math.max(0, kmDriven);
        this.speed = 0.0;
    }

    public String getBrand() {
        return brand;
    }

    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    public int getKmDriven() {
        return kmDriven;
    }

    public void setKmDriven(int kmDriven) {
        this.kmDriven = kmDriven;
    }

    public double getSpeed() {
        return speed;
    }

    public void accelerate(double acceleration) {
        if (acceleration > 0) {
            this.speed += acceleration;
        }
        return;

    }

    public void brake(double brake) {
        if (this.speed - brake < 0 || brake < 0) {
            throw new IllegalArgumentException("Cant brake this amount");
        } else {
            speed = Math.max(0, this.getSpeed() - brake);
        }
    }

    @Override
    public String toString() {
        return "Car [brand=" + brand + ", model=" + model + ", year=" + year + ", kmDriven=" + kmDriven + "]";
    }

    public static void main(String[] args) {
        Car c1 = new Car("Tesla", "Model S", 2019, 1000);

        System.out.println(c1);

        c1.accelerate(100);
        c1.brake(41.6);
    }
}
