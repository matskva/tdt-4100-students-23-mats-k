package of1.kode;

public class Book {

    String title;
    int pages;

    Book(String title, int pages) {
        setPages(pages);
        setTitle(title);

    }

    String getTitle() {
        return title;
    }

    void setTitle(String title) {
        this.title = title;
    }

    int getPages() {
        return pages;
    }

    void setPages(int pages) {
        if (pages <= 0) {
            this.pages = 0;
        } else {
            this.pages = pages;

        }
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "The book \"" + title + "\" has " + pages + " pages";
    }

    public static void main(String[] args) {
        Book b1 = new Book("Big java", 100);
        Book b2 = new Book("Into to algorithms", 700);

        System.out.println(b1);

        b1.setTitle("Another Book");
        b1.setPages(250);
        System.out.println(b1);

        b1.setPages(-100);
        System.out.println(b1);

    }
}
