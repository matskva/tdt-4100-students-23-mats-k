package uke13.pizza.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import uke13.pizza.model.toppingdecorators.Cheese;
import uke13.pizza.model.toppingdecorators.Ham;
import uke13.pizza.model.toppingdecorators.Mushroom;
import uke13.pizza.model.toppingdecorators.Olives;
import uke13.pizza.model.toppingdecorators.Onions;
import uke13.pizza.model.toppingdecorators.Pepperoni;
import uke13.pizza.model.toppingdecorators.Pineapple;
import uke13.pizza.model.toppingdecorators.ITopping;
import uke13.pizza.util.PizzaCosts;




public class PizzaTest {

    Pizza pizza = new BasicPizza();

    

    // @BeforeAll




/*
 *      availableToppings.add(new Cheese(2.5));
        availableToppings.add(new Ham(3.0));
        availableToppings.add(new Mushroom(1.5));
        availableToppings.add(new Olives(1.5));
        availableToppings.add(new Onions(1.0));
        availableToppings.add(new Pepperoni(2.0));
        availableToppings.add(new Pineapple(2.0));
 */

    @Test
    public void testNoToppingHasBasicDescription() {
        assertEquals("Basic Pizza", pizza.getDescription());
    }

    @Test
    public void testNoToppingHasBasicValue() {
        assertEquals(PizzaCosts.BasicPizza, pizza.getCost());
    }

    @Test
    public void testWithCheeseToppingHasOkayDescription() {
        pizza = pizza.addTopping(new Cheese(PizzaCosts.CHEESE));
        assertEquals("Basic Pizza, Cheese", pizza.getDescription());
    }

    @Test
    public void testToppingWithCheeseHasBasicValue() {
        pizza = pizza.addTopping(new Cheese(PizzaCosts.CHEESE));
        assertEquals(PizzaCosts.BasicPizza+PizzaCosts.CHEESE, pizza.getCost());
    }

    @Test
    public void testCanOrderTwoCheeses() {
        pizza = pizza.addTopping(new Cheese(PizzaCosts.CHEESE));
        pizza = pizza.addTopping(new Cheese(PizzaCosts.CHEESE));
        assertEquals("Basic Pizza, Cheese, Cheese", pizza.getDescription());
        assertEquals(PizzaCosts.BasicPizza+PizzaCosts.CHEESE*2, pizza.getCost());
    }

    @Test
    public void testToppingsAddedInRightOrder() {
        pizza = pizza.addTopping(new Cheese(PizzaCosts.CHEESE));
        pizza = pizza.addTopping(new Cheese(PizzaCosts.CHEESE));
        pizza = pizza.addTopping(new Ham(PizzaCosts.HAM));
        // From pizza
        List<ITopping> pizzatoppings = new ArrayList<>(pizza.getToppings());        
        System.out.println(pizzatoppings);
        // made in test lab:
        List<ITopping> testToppings = new ArrayList<>();
        testToppings.add(new Cheese(PizzaCosts.CHEESE));
        testToppings.add(new Cheese(PizzaCosts.CHEESE));
        testToppings.add(new Ham(PizzaCosts.HAM));
        System.out.println(testToppings);

        for (int i = 0; i < pizzatoppings.size(); i++) {
            assertEquals(pizzatoppings.get(i).getName(), testToppings.get(i).getName());
            assertEquals(pizzatoppings.get(i).getCost(), testToppings.get(i).getCost());
        }
    }

    @Test
    public void testAllToppingsAdded() {
        pizza = pizza.addTopping(new Cheese(PizzaCosts.CHEESE))
        .addTopping(new Ham(PizzaCosts.HAM))
        .addTopping(new Mushroom(PizzaCosts.MUSHROOM))
        .addTopping(new Olives(PizzaCosts.OLIVES))
        .addTopping(new Onions(PizzaCosts.ONIONS))
        .addTopping(new Pepperoni(PizzaCosts.PEPPERONI))
        .addTopping(new Pineapple(PizzaCosts.PINEAPPLE));
        System.out.println(pizza.getCost());
        assertEquals( 
        PizzaCosts.BasicPizza+PizzaCosts.CHEESE+
        PizzaCosts.HAM+PizzaCosts.MUSHROOM+PizzaCosts.OLIVES+
        PizzaCosts.ONIONS+PizzaCosts.PEPPERONI+PizzaCosts.PINEAPPLE, pizza.getCost());
    }

}

