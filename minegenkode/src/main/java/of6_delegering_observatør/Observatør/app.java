package of6_delegering_observatør.Observatør;

import java.util.ArrayList;
import java.util.List;

public class app {

    List<Applistener> subscribers = new ArrayList<>();

    public void subrscribe(Applistener listener) {
        if (!this.subscribers.contains(listener)) {
            this.subscribers.add(listener);
        }
    }

    public void unsubrscirbe(Applistener listener) {
        if (subscribers.contains(listener)) {
            this.subscribers.remove(listener);
        }
    }

    @Override
    public void sendPushNotification(String notification) {
        this.subscribers.forEach(sub -> sub.receivePushNotification(notification));
    }

}
