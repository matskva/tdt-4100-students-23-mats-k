package of6_delegering_observatør.Observatør;

import java.util.ArrayList;
import java.util.List;

public class Phone implements Applistener {

    List<String> notification = new ArrayList<>();
    List<app> subscribing = new ArrayList<>();

    @Override
    public void receivePushNotification(String notification) {
        this.notification.add(notification);
        System.out.println(notification);
    }

    public void subrscribe(app app) {
        if (!this.subscribing.contains(app)) {
            this.subscribing.add(app);
            app.subrscribe(this);
        }
    }

    public void unsubrscirbe(app app) {
        if (subscribing.contains(app)) {
            this.subscribing.remove(app);
            app.unsubrscirbe(this);
        }
    }

}
