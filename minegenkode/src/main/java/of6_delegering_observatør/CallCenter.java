package of6_delegering_observatør;

import java.util.ArrayList;
import java.util.List;

public class CallCenter implements CallRecipient {

    private List<Employee> employees = new ArrayList<>();

    @Override
    public void answerCall(String call) {
        String role;
        if (call.equals("klage")) {
            role = "Junior";
        } else if (call.equals("Internt")) {
            role = "HR";
        } else if (call.equals("advanced")) {
            role = "teamleder";
        }

        else {
            role = "junior";
        }

        this.getEmployeeForTask(role).answerCall(call);
    }

    public void addEmployee(Employee employee) {

        if (!this.employees.contains(employee)) {
            this.employees.add(employee);
        }
    }

    private Employee getEmployeeForTask(String role) {
        return this.employees.stream()
                .filter(employee -> employee.getRole().equals(role))
                .min((employee1, employee2) -> employee1.getNumberOfCalls() - employee2.getNumberOfCalls())
                .get();

    }

    
}
