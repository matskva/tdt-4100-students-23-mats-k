package of6_delegering_observatør;

public interface CallRecipient {
    void answerCall(String call);
}
