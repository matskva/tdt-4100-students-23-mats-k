package of.of2;

import java.util.Arrays;
import java.util.List;

public class selfServiceCheckout {
    static final List<String> days = Arrays.asList("mon", "tue", "wed", "thu", "fri", "sat", "sun");

    private String day;
    private String phoneNumber;

    public selfServiceCheckout(String day, String number) {
        validateDay(day);
        this.day = day;
        validPhoneNumber(number);
        this.phoneNumber = number;
    }

    private void validateDay(String day) {

        if (!days.contains(day)) {
            throw new IllegalArgumentException("Not a valid day");
        }
    }

    public void setPhoneNumber(String number) {
        if (!validPhoneNumber(number)) {
            throw new IllegalArgumentException("Invalid phone number");
        } else {
            this.phoneNumber = number;
        }
    }

    private boolean validPhoneNumber(String number) {

        String cleanPhoneNumber = number.replaceAll("\\s", "");

        if (cleanPhoneNumber.startsWith("0047")) {
            if (!(cleanPhoneNumber.startsWith("00479", 0) || cleanPhoneNumber.startsWith("00474"))) {
                return false;

            }
            if (cleanPhoneNumber.length() != 12) {
                return false;
            }
        } else if (cleanPhoneNumber.startsWith("+47")) {
            if (!(cleanPhoneNumber.startsWith("+479", 0) || cleanPhoneNumber.startsWith("+474"))) {
                return false;

            }
            if (cleanPhoneNumber.length() != 11) {
                return false;
            }
        } else {
            return false;
        }

        String subCleanPhoneNumber = cleanPhoneNumber.substring(cleanPhoneNumber.length() - 8);

        char[] chars = subCleanPhoneNumber.toCharArray();

        for (char c : chars) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        SelfServiceCheckout c1 = new SelfServiceCheckout("mon", "+4690478166");

    }
}
