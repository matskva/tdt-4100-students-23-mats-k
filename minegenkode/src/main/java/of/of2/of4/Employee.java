package of.of2.of4;

public class Employee {

    private Department employer;

    Employee(Department dep) {
        this.employer = dep;
        this.employer.addEmployee(this);
    }

    public void promote() {
        if (this.employer.getParentDepartment() == null) {
            throw new IllegalArgumentException("No higher department to be promoted to!");
        }

        Department newJob = this.employer.getParentDepartment();
        this.employer.removeEmployee(this);
        this.employer = newJob;
        this.employer.addEmployee(this);

    }

    public void quit() {

        if (this.employer == null) {
            throw new IllegalStateException("You don't have an employer to quit"); // state exception er brukt når det
                                                                                   // er tilstanden det er noe feil med,
                                                                                   // ikke hvilket argument som blir
                                                                                   // passert inn.
        }

        this.employer.removeEmployee(this);
        this.employer = null;
    }
}
