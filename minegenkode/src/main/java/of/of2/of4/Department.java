package of.of2.of4;

import java.util.ArrayList;
import java.util.Collection;

public class Department {

    private Department parentDepartment;
    Collection<Department> SubDepartment = new ArrayList<>();
    Collection<Employee> employees = new ArrayList<>();

    public Department(Department parentDepartment) {
        this.parentDepartment = parentDepartment;
        this.parentDepartment.setSubDepartment(this);
    }

    public Department() {

    }

    private void setSubDepartment(Department department) {

        if (this.SubDepartment.contains(department)) {
            throw new IllegalArgumentException("Department already a subdepartment!");
        }

        this.SubDepartment.add(department);

    }

    public boolean containsDepartment(Department other) {
        if (this.parentDepartment == null) {
            return false;
        }

        for (Department dep : this.SubDepartment) {
            if (dep == other || dep.SubDepartment.contains(other)) {
                return true;
            }
        }
        return false;
    }

    public Department getParentDepartment() {
        return this.parentDepartment;
    }

    public void addEmployee(Employee employee) {

        if (this.employees.contains(employee)) {
            throw new IllegalArgumentException("Employee already a employed!");
        }

        this.employees.add(employee);

    }

    public void removeEmployee(Employee emp) {
        if (!this.employees.contains(emp)) {
            throw new IllegalArgumentException("Employe not employed at department");
        }
        this.employees.remove(emp);

    }

    public static void main(String[] args) {
        Department test = new Department();
    }
}
